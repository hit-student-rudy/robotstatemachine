#!/usr/bin/env python3
import rospy
import threading

from warning_message.msg import warning  

def WarningsPublisher():
    audioWarn_publisher = rospy.Publisher('/audio_request',warning, queue_size=1)
    return audioWarn_publisher

def PublishWarning(wp_num, priority_lev, audiofile, speech_string, publisher):
    warn_msgs = warning()
    warn_msgs.timestamp = rospy.get_rostime()
    warn_msgs.workpackage_number = wp_num
    warn_msgs.priority_level = priority_lev
    warn_msgs.audiofilename = audiofile
    warn_msgs.speech_string = speech_string
    publisher.publish(warn_msgs)

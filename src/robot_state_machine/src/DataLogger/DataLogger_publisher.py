#!/usr/bin/env python3

import rospy
import threading

from std_msgs.msg import Header


def DataPublisher():
    data_publisher = rospy.Publisher('/kkh_data_logger',Header, queue_size=1)
    return data_publisher

def DataLogging(log_string, publisher):
    log_msgs = Header()
    log_msgs.stamp = rospy.get_rostime()
    log_msgs.frame_id = log_string
    publisher.publish(log_msgs)

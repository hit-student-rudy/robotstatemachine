#!/usr/bin/env python

import threading
import rospy
import actionlib

import os
import subprocess


from std_msgs.msg import Empty, Int64, Bool, Header, String

from warning_message.msg import warning 

from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from geometry_msgs.msg import PoseWithCovarianceStamped, Twist
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal

# from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
# from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from math import sqrt

class LiftRailing():

    def __init__(self):
        # rospy.init_node('lift_railing_motion')
        
        self.head_controller_topic = '/head_controller/command'
        self.arm_controller_topic = '/arm_controller/command'
        self.torso_controller_topic = '/torso_controller/command'

        self.frame_id = rospy.get_param('~goal_frame_id','map')
        
        self.movement_pub = rospy.Publisher('/mobile_base_controller/cmd_vel', Twist, queue_size=1)
        
       # self.client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
       # rospy.loginfo('Connecting to move_base...')
       # self.client.wait_for_server()
       # rospy.loginfo('Connected to move_base.')

        self.lift_motion = True
       

    def start_controllers(self):

        # Start head controller
        os.system(" rosservice call /controller_manager/load_controller 'name: 'head_controller'' ")
        rospy.sleep(0.5)
        os.system("rosservice call /controller_manager/switch_controller '{start_controllers: ['head_controller']}' ")
        rospy.sleep(0.5)

        # Start arm_controller
        os.system(" rosservice call /controller_manager/load_controller 'name: 'arm_controller'' ")
        rospy.sleep(0.5)
        os.system("rosservice call /controller_manager/switch_controller '{start_controllers: ['arm_controller']}' ")
        rospy.sleep(0.5)

        # Start torso_controller
        os.system(" rosservice call /controller_manager/load_controller 'name: 'torso_controller'' ")
        rospy.sleep(0.5)
        os.system("rosservice call /controller_manager/switch_controller '{start_controllers: ['torso_controller']}' ")
        rospy.sleep(0.5)
        rospy.loginfo("Controllers loaded")

    def stop_controllers(self):

        # Stop head controller
        os.system("rosservice call /controller_manager/switch_controller '{stop_controllers: ['head_controller']}' ")
        rospy.sleep(0.5)
        os.system(" rosservice call /controller_manager/unload_controller 'name: 'head_controller'' ")
        rospy.sleep(0.5)
        
        # Stop arm_controller
        os.system("rosservice call /controller_manager/switch_controller '{stop_controllers: ['arm_controller']}' ")
        rospy.sleep(0.5)
        os.system(" rosservice call /controller_manager/unload_controller 'name: 'arm_controller'' ")
        rospy.sleep(0.5)
        
        # Start torso_controller
        os.system("rosservice call /controller_manager/switch_controller '{stop_controllers: ['torso_controller']}' ")
        rospy.sleep(0.5)
        os.system(" rosservice call /controller_manager/unload_controller 'name: 'torso_controller'' ")
        rospy.sleep(0.5)
        rospy.loginfo("Controllers unloaded")


    def lift_motion_callback(self, event):
        print("Time is passed")
        self.lift_motion = False
    
    
    def Publish_Joints_Positions(self, joint_names, joint_positions, publish_on_topic):
        pub = rospy.Publisher(publish_on_topic, JointTrajectory, queue_size=1)
        traj_msgs = JointTrajectory()
        traj_msgs.header.stamp = rospy.Time.now()
        traj_msgs.header.frame_id = ''
        traj_msgs.joint_names = joint_names
        num_of_joints = len(joint_names)
        traj_msgs.points =[JointTrajectoryPoint(positions=joint_positions, velocities=[0]*num_of_joints, time_from_start=rospy.Duration(5.0))]
        pub.publish(traj_msgs)

    def lift_motion_configuration(self):

        joint_names = { 'head_joints': ['head_1_joint', 'head_2_joint'],
                        'arm_joints': ['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint', 'arm_7_joint'],
                        'torso_joint' : ['torso_lift_joint'] }
        
        joint_positions = {'head_pos' : [0,-0.5],
                           'arm_pos' : [0.65, -1.60, -2.2, 1.9, 1.5, 1.3, -1.458],
                           'torso_pos' : [0.1] } # min is 0.0

        return joint_names, joint_positions

    def correct_eof(self):

        joint_names = { 'head_joints': ['head_1_joint', 'head_2_joint'],
                        'arm_joints': ['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint', 'arm_7_joint'],
                        'torso_joint' : ['torso_lift_joint'] }
        
        joint_positions = {'head_pos' : [0,-0.5],
                           'arm_pos' : [0.65, -1.60, -2.2, 1.9, 1.5, 0.3, -1.458],
                           'torso_pos' : [0.1] } # min is 0.0

        return joint_names, joint_positions

    def navigation_configuration(self):

        joint_names = { 'head_joints': ['head_1_joint', 'head_2_joint'],
                        'arm_joints': ['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint', 'arm_7_joint'],
                        'torso_joint' : ['torso_lift_joint'] }
        
        joint_positions = {'head_pos' : [0,-0.5],
                           'arm_pos' : [0.2, -1.33, -0.2, 1.93, -1.57, 1.36, 0.0],
                           'torso_pos' : [0.3] } # max is 0.34

        return joint_names, joint_positions
    
    def lift_up_rail(self):
        joint_names = { 'head_joints': ['head_1_joint', 'head_2_joint'],
                        'arm_joints': ['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint', 'arm_7_joint'],
                        'torso_joint' : ['torso_lift_joint'] }
        
        joint_positions = {'head_pos' : [0,-0.5],
                           'arm_pos' : [0.65, -1.60, -2.2, 1.9, 1.5, 0.3, -1.458],
                           'torso_pos' : [0.3] }

        return joint_names, joint_positions

    def retract_arm(self):
        joint_names = { 'head_joints': ['head_1_joint', 'head_2_joint'],
                        'arm_joints': ['arm_1_joint', 'arm_2_joint', 'arm_3_joint', 'arm_4_joint', 'arm_5_joint', 'arm_6_joint', 'arm_7_joint'],
                        'torso_joint' : ['torso_lift_joint'] }
        
        joint_positions = {'head_pos' : [0,-0.5],
                           'arm_pos' : [0.65, -1.60, -2.2, 0.7, 1.5, 0.3, -1.458],
                           'torso_pos' : [0.25] }

        return joint_names, joint_positions


    def ReachLiftConfig(self):
        self.lift_motion = True
        self.start_controllers()
        timer = rospy.Timer(rospy.Duration(10), self.lift_motion_callback)
        [joint_names, joint_positions] = self.lift_motion_configuration()
        rospy.loginfo("Reaching lift railing configuration...")
        while self.lift_motion:
            self.Publish_Joints_Positions( joint_names['head_joints'], joint_positions['head_pos'],self.head_controller_topic)
            self.Publish_Joints_Positions( joint_names['arm_joints'], joint_positions['arm_pos'],self.arm_controller_topic)
            self.Publish_Joints_Positions( joint_names['torso_joint'], joint_positions['torso_pos'],self.torso_controller_topic)
            rospy.sleep(0.1)
        timer.shutdown()
        rospy.loginfo("Configuration reached!")
        # self.stop_controllers()
        rospy.loginfo("Controllers stopped and umloaded!")
        rospy.loginfo("Will move slightly in front!")
    
    def CorrectEOF(self):
        self.lift_motion = True
        timer = rospy.Timer(rospy.Duration(3), self.lift_motion_callback)
        [joint_names, joint_positions] = self.correct_eof()
        rospy.loginfo("Correct eof pose...")
        while self.lift_motion:
            self.Publish_Joints_Positions( joint_names['head_joints'], joint_positions['head_pos'],self.head_controller_topic)
            self.Publish_Joints_Positions( joint_names['arm_joints'], joint_positions['arm_pos'],self.arm_controller_topic)
            self.Publish_Joints_Positions( joint_names['torso_joint'], joint_positions['torso_pos'],self.torso_controller_topic)
            rospy.sleep(0.1)
        timer.shutdown()
        rospy.loginfo("Configuration reached!")
        # self.stop_controllers()
       
    def LiftUpRailing(self):
        self.lift_motion = True
        timer = rospy.Timer(rospy.Duration(10), self.lift_motion_callback)
        [joint_names, joint_positions] = self.lift_up_rail()
        while self.lift_motion:
            self.Publish_Joints_Positions( joint_names['head_joints'], joint_positions['head_pos'],self.head_controller_topic)
            self.Publish_Joints_Positions( joint_names['arm_joints'], joint_positions['arm_pos'],self.arm_controller_topic)
            self.Publish_Joints_Positions( joint_names['torso_joint'], joint_positions['torso_pos'],self.torso_controller_topic)
            rospy.sleep(0.1)
        timer.shutdown()
        rospy.loginfo("Configuration reached!")

    def ReachNavigationConfig(self):
        self.lift_motion = True
        # self.start_controllers()
        timer = rospy.Timer(rospy.Duration(15), self.lift_motion_callback)
        [joint_names, joint_positions] = self.navigation_configuration()
        rospy.loginfo("Reaching navigation configuration...")
        while self.lift_motion:
            self.Publish_Joints_Positions( joint_names['torso_joint'], joint_positions['torso_pos'],self.torso_controller_topic)
            self.Publish_Joints_Positions( joint_names['head_joints'], joint_positions['head_pos'],self.head_controller_topic)
            self.Publish_Joints_Positions( joint_names['arm_joints'], joint_positions['arm_pos'],self.arm_controller_topic)
            rospy.sleep(0.0)
        timer.shutdown()
        rospy.loginfo("Configuration reached!")
        # self.stop_controllers()
        os.system("rosservice call /move_base/clear_costmaps '{}'")
        rospy.sleep(1.0)
        rospy.loginfo("Controllers stopped and unloaded!")   

    
    def MoveForwardToLift(self):
        current_pose = rospy.wait_for_message('/amcl_pose', PoseWithCovarianceStamped, timeout=2)
        os.system("rosservice call /move_base/clear_costmaps '{}'")
        rospy.sleep(1.0)
        # rospy.loginfo('Iam at: %s',(current_pose.pose.pose.position.x) )
        # goal = MoveBaseGoal()
        # goal.target_pose.header.frame_id = self.frame_id
        # goal.target_pose.pose.position.x = current_pose.pose.pose.position.x + 0.15
        # goal.target_pose.pose.position.y = current_pose.pose.pose.position.y
        # goal.target_pose.pose.position.z = current_pose.pose.pose.position.z
        # goal.target_pose.pose.orientation = current_pose.pose.pose.orientation

        # self.client.send_goal(goal)
        # self.client.wait_for_result()

        ## move forward
        start =  rospy.Time.now()
        move = Twist()
        while (rospy.Time.now() - start < rospy.Duration(6.0)):
            rospy.loginfo("Moving forward")
            ## velocity controls
            move.linear.x = 0.05 
            move.angular.z = 0
            self.movement_pub.publish(move)
            rospy.sleep(0.1)
        rospy.sleep(1.0)
        rospy.loginfo("Now I am ready to lift!")
    
    def RetractArm(self):
        self.lift_motion = True
        # self.start_controllers()
        timer = rospy.Timer(rospy.Duration(5), self.lift_motion_callback)
        [joint_names, joint_positions] = self.retract_arm()
        rospy.loginfo("Retracting arm...")
        while self.lift_motion:
            self.Publish_Joints_Positions( joint_names['torso_joint'], joint_positions['torso_pos'],self.torso_controller_topic)
            self.Publish_Joints_Positions( joint_names['head_joints'], joint_positions['head_pos'],self.head_controller_topic)
            self.Publish_Joints_Positions( joint_names['arm_joints'], joint_positions['arm_pos'],self.arm_controller_topic)
            rospy.sleep(0.1)
        timer.shutdown()
        rospy.sleep(1.0)
        rospy.loginfo("Arm retracted")


    def MoveBackwardToNav(self):
        current_pose = rospy.wait_for_message('/amcl_pose', PoseWithCovarianceStamped, timeout=2)
        os.system("rosservice call /move_base/clear_costmaps '{}'")
        rospy.sleep(1.0)
        
        ## Move backwards
        start =  rospy.Time.now()
        move = Twist()
        while (rospy.Time.now() - start < rospy.Duration(6.0)):
            rospy.loginfo("Moving backward")
            ## velocity controls
            move.linear.x = -0.05 
            move.angular.z = 0
            self.movement_pub.publish(move)
            rospy.sleep(0.1)
        rospy.sleep(1.0)
        rospy.loginfo("I ll go back to nav configuration!")

    def PerformLiftBedRailing(self):
        self.ReachLiftConfig()
        self.CorrectEOF()
        self.MoveForwardToLift()
        self.LiftUpRailing()
        rospy.sleep(5.0)
        self.RetractArm()
        self.MoveBackwardToNav()
        self.ReachNavigationConfig()
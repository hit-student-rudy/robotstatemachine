#!/usr/bin/env python

import threading
import rospy
import actionlib

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseArray, PoseStamped
from std_msgs.msg import Empty, Int64, Bool, Header
from actionlib_msgs.msg import GoalID

 

def setup_task_variables(self):
    
    self.waypoints = []
    self.last_waypoint_index = []
    self.start_index = []
    self.current_waypoint_index = 0
    self.next_waypoint_index = 0
    self.index = 0
    self.skip = False
    self.start_over = False
    self.stop_ = False
    self.CurrentGoal_ = []
    
#!/usr/bin/env python
import rospy
import threading

from diginova_msgs.msg import DNTextmessage
from std_msgs.msg import String

class TTSPublisher():

    def __init__(self):
        self.ttsmessage_publisher = rospy.Publisher('/DN_tts_msgs', DNTextmessage, queue_size=1)

    def PublishTTSText(self, text):

        stream_id = rospy.wait_for_message('/DNStream_ID',String,timeout=None)
        text_msgs = DNTextmessage()
        text_msgs.Type   = "Robot2Server"
        text_msgs.RobotID = "00018001"
        text_msgs.RobotName = "rose-hit001"
        text_msgs.ClientID = "0001"
        text_msgs.ClientName = "diginova"
        text_msgs.MessageID = "MS1015706070710001"
        text_msgs.Time = "2019-04-30 08:37:46"
        text_msgs.GroupName = "mijnbouwstraat"
        text_msgs.EventName = "CloudTextToSpeech"
        text_msgs.EventData = ' ' 
        text_msgs.Engine = 'prolody'
        text_msgs.EngineParameters = ' '
        text_msgs.Text = text
        text_msgs.StreamID = stream_id.data.replace('"', '')
        text_msgs.Language = 'nl-NL'
        text_msgs.Voice = 'female'
        text_msgs.EventID = 'MS10'

        while not rospy.is_shutdown():
           connections = self.ttsmessage_publisher.get_num_connections()
           rospy.loginfo("Connections; %d", connections)
           if connections > 0:
               self.ttsmessage_publisher.publish(text_msgs)
               break
           rospy.sleep(0.01)

    def PublishWarning(self, text):
        self.PublishTTSText(text)
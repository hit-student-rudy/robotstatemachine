#!/usr/bin/env python

import threading
import rospy
import actionlib

from smach import State
from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseArray, PoseStamped
from std_msgs.msg import Empty, Int64, Bool, Header
from actionlib_msgs.msg import GoalID
from visualization_msgs.msg import Marker

from waypoints_msgs.msg import Waypoints
from warning_message.msg import warning

from std_srvs.srv import Empty, EmptyRequest

from TTSMessages.TTSPublisher import TTSPublisher

class GoInFrontOfBed(State): 

    def __init__(self):
        State.__init__(self, outcomes=['success', 'need_help', 'out'])

        self.data_publisher = DataPublisher()
        self.stopSignal_publisher = rospy.Publisher('/start_cls', Bool, queue_size=1)
        self.start_cls=Bool()
        
        self.frame_id = rospy.get_param('~goal_frame_id','map')
        # Get a move_base action client 
        self.client = actionlib.SimpleActionClient('move_base', MoveBaseAction)

        self.BedRailing_pub_ = rospy.Publisher('/Bed_railing_Label', Marker, queue_size=1)
        self.BedRailingShape_pub_ = rospy.Publisher('/Bed_railing_Point', Marker, queue_size=1)

        rospy.wait_for_service('/move_base/clear_costmaps')
        self.clear_costmaps_srv = rospy.ServiceProxy('/move_base/clear_costmaps', Empty)
        
        self.bed_publisher = TTSPublisher()

    def execute(self, userdata):

        rospy.loginfo("Entered GoInFrontOfBed State")
        DataLogging('Entered GoInFrontOfBed State', self.data_publisher)

        self.bed_publisher.PublishWarning("Ik heb een bed gevonden!")
        
        bed_railing_topic = '/i_found_a_bed'
        hrz_topic = '/hrz_triggered'
        go_in_front_of_bed = True
        self.start_cls.data = False

        teleop = Bool()
        teleop.data = True
        
        bed_railing_pose = PoseWithCovarianceStamped()
        bed_railing_pose.header.frame_id = self.frame_id
        bed_railing_pose.header.stamp = rospy.Time.now()
        bed_railing_pose.pose.pose.position.x = -0.05
        bed_railing_pose.pose.pose.position.y = 2.57
        bed_railing_pose.pose.pose.position.z =  0.0
        bed_railing_pose.pose.pose.orientation.x =  0.0
        bed_railing_pose.pose.pose.orientation.y =  0.0
        bed_railing_pose.pose.pose.orientation.z =  0.18
        bed_railing_pose.pose.pose.orientation.w =  0.99     
        bed_railing = Marker()
        bed_railing.header.frame_id = bed_railing_pose.header.frame_id
        bed_railing.header.stamp = rospy.Time.now()
        bed_railing.ns = 'hospital_bed'
        bed_railing.id = 0
        bed_railing.type = bed_railing.TEXT_VIEW_FACING
        bed_railing.action = bed_railing.ADD
        bed_railing.pose.position.x = 0.97 # bed_railing_pose.pose.pose.position.x
        bed_railing.pose.position.y = 2.87 #bed_railing_pose.pose.pose.position.y
        bed_railing.pose.position.z = 0.5
        bed_railing.pose.orientation.w= 1.0
        bed_railing.scale.x = 0.6
        bed_railing.scale.y = 0.6
        bed_railing.scale.z = 0.6
        bed_railing.color.a = 1.0
        bed_railing.color.r = 0.8
        bed_railing.color.g = 0.3
        bed_railing.color.b = 0.0
        bed_railing.text = "Bed_Railing"

        bed_railing_shape = Marker()
        bed_railing_shape.header.frame_id = bed_railing_pose.header.frame_id
        bed_railing_shape.header.stamp = rospy.Time.now()
        bed_railing_shape.ns = 'bed_railing'
        bed_railing_shape.id = 1
        bed_railing_shape.type = bed_railing.SPHERE
        bed_railing_shape.action = bed_railing.ADD
        bed_railing_shape.pose.position.x = 0.97 #bed_railing_pose.pose.pose.position.x + 0.9
        bed_railing_shape.pose.position.y = 2.87 # bed_railing_pose.pose.pose.position.y
        bed_railing_shape.pose.position.z = 0.0
        bed_railing_shape.pose.orientation.w= 1.0
        bed_railing_shape.scale.x = 0.5
        bed_railing_shape.scale.y = 0.5
        bed_railing_shape.scale.z = 0.5
        bed_railing_shape.color.a = 1.0
        bed_railing_shape.color.r = 0.8
        bed_railing_shape.color.g = 0.3
        bed_railing_shape.color.b = 0.0

        self.BedRailing_pub_.publish(bed_railing)
        self.BedRailingShape_pub_.publish(bed_railing_shape)
        self.stopSignal_publisher.publish(self.start_cls)

        attempts = 0

        while go_in_front_of_bed:

            try:
                active = rospy.wait_for_message(bed_railing_topic, Bool, timeout=1)
                rospy.loginfo('BED: %s',(active.data))
            except rospy.ROSException as e:
                if 'timeout exceeded' in e.message:
                    continue  # no new waypoint within timeout, looping...
                else:
                    raise e

            goal = MoveBaseGoal()
            goal.target_pose.header.frame_id = self.frame_id
            goal.target_pose.pose = bed_railing_pose.pose.pose
            
            rospy.loginfo("ATTEMPTS: %s", (attempts) )

            if attempts >= 3:
                self.bed_publisher.PublishWarning("Oeps, Ik kan niet naar de bed gaan, Kunt u mee helpen?")
                rospy.sleep(3.0)
                return 'need_help'
            else:
                pass

            self.client.send_goal(goal)
            
            self.client.wait_for_result()
            print(self.client.get_state())
            if self.client.get_state() == 3: 
                # PublishWarning(1, 40, 'Coffee machine!', 'Coffee machine!', self.audioWarn_publisher)
                # DataLogging('Coffee machine!', self.data_publisher)
                go_in_front_of_bed = False
                rospy.loginfo("Reached bed. I am gonna take lifting position")
                rospy.sleep(2.0)       
                return 'success'
            elif self.client.get_state() == 2: 
                hrz = rospy.wait_for_message(hrz_topic, Bool, timeout=1)
                if (hrz.data):
                    attempts = 0
                    return 'out' 
                else:
                    pass
                rospy.loginfo('Bed looks occupied. I ll attempt again!')
                self.clear_costmaps_srv(EmptyRequest())
                rospy.sleep(3.0)
                attempts = attempts +1
            elif self.client.get_state() == 4: 
                rospy.loginfo('Bed looks occupied. I ll attempt again!')
                self.clear_costmaps_srv(EmptyRequest())
                rospy.sleep(3.0)
                attempts = attempts +1
            else:
                attempts = 0
                if active.data == False:
                    go_in_front_of_bed = False
                else:
                    go_in_front_of_bed = True
        rospy.loginfo("Exiting GoInFrontOfBed State..")
        rospy.sleep(2.0)        
        return 'success'

#!/usr/bin/env python

import threading
import rospy
import actionlib

from smach import State
from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseArray, PoseStamped
from std_msgs.msg import Empty, Int64, Bool, Header
from actionlib_msgs.msg import GoalID

# from Setup_Variables.variables import *

from waypoints_msgs.msg import Waypoints
from warning_message.msg import warning

# Import LiftRailing motions
from LiftRailing import LiftRailing


class CoffeeTeleoperation(State): 

    def __init__(self):
        State.__init__(self, outcomes=['success'])

        self.data_publisher = DataPublisher()
        self.teleop_active = True
        self.stopSignal_publisher = rospy.Publisher('/start_cls', Bool, queue_size=1)
        self.start_cls=Bool()
        
        self.Controllers = LiftRailing.LiftRailing()


    def execute(self, userdata):
        rospy.loginfo("Entered CoffeeTeleoperation State")
        DataLogging('Entered CoffeeTeleoperation State', self.data_publisher)
        
        self.Controllers.stop_controllers()

        teleop_topic = '/coffee_teleop'
        self.teleop_active = True
        self.start_cls.data = False

        while self.teleop_active:
            try:
                active = rospy.wait_for_message(teleop_topic, Bool, timeout=1)
                rospy.loginfo('COFFEE_TELEOP: %s',(active.data))
            except rospy.ROSException as e:
                if 'timeout exceeded' in e.message:
                    continue  # no new waypoint within timeout, looping...
                else:
                    raise e
            if active.data == False:
                    self.teleop_active = False
            else:
                    self.teleop_active = True
            self.stopSignal_publisher.publish(self.start_cls)
        rospy.loginfo("Exiting CoffeeTeleoperation State..")
        self.Controllers.start_controllers()
        rospy.loginfo("Reach Navigation Config")
        self.Controllers.ReachNavigationConfig()
        rospy.sleep(2.0)
        rospy.loginfo("FINISHED")         
        return 'success'

#!/usr/bin/env python

import threading
import rospy
import actionlib

from smach import State
from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseArray, PoseStamped
from std_msgs.msg import Empty, Int64, Bool, Header
from actionlib_msgs.msg import GoalID

from waypoints_msgs.msg import Waypoints
from warning_message.msg import warning

class Stop(State):
    def __init__(self):
        State.__init__(self, outcomes=['success'])
        self.data_publisher = DataPublisher()
        
    
    def execute(self, userdata):
        rospy.loginfo("Shutting down the state machine")
        DataLogging('State Machine shut down!', self.data_publisher)
        rospy.sleep(2.0)
        return 'success'

        
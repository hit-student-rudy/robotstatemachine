#!/usr/bin/env python

import threading
import rospy
import actionlib

from smach import State
from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped, Pose, PoseArray, PoseStamped
from std_msgs.msg import Empty, Int64, Bool, Header
from actionlib_msgs.msg import GoalID

# from Setup_Variables.variables import *

from waypoints_msgs.msg import Waypoints
from warning_message.msg import warning

from math import sqrt


def euclidean_distance(current_pose, current_goal):
    """Euclidean distance between current pose and the goal."""
    return sqrt(pow((current_pose.pose.pose.position.x - current_goal.target_pose.pose.position.x), 2) +
                pow((current_pose.pose.pose.position.y - current_goal.target_pose.pose.position.y), 2))


class SkipWaypoint(State):
    
    # Here we have to count for the clearances and if they overcome a threshold to move to the next waypoint 
    def __init__(self,start_over):
        State.__init__(self, outcomes=['success','object_moved','teleop','get_coffee','bed_found','human_found'], input_keys=['last_waypoint_index','SW_waypoints_in'], output_keys=['next_waypoint_index'])
        self.client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
        rospy.loginfo('Connecting to move_base SKIP...')
        self.client.wait_for_server()
        rospy.loginfo('Connected to move_base.')
        self.audioWarn_publisher = WarningsPublisher()
        self.data_publisher = DataPublisher()
        self.start_over = start_over

        
    def execute(self, userdata):
        stop_topic = "/amcl_pose"
        clear_topic = "/clearances"
        waypoints_topic = "/waypoints" 

        skip_topic = "/Goal_to_skip"
        CurrentGoal = rospy.wait_for_message(skip_topic, MoveBaseGoal,  timeout=1) 

        rospy.loginfo('Goal entered:')
        print(CurrentGoal)

        
        waypoints = rospy.wait_for_message(waypoints_topic, PoseArray, timeout=1) 
       
        waypoint = Pose()
        waypoint.position = CurrentGoal.target_pose.pose.position
        waypoint.orientation = CurrentGoal.target_pose.pose.orientation
        current_index = waypoints.poses.index(waypoint)

        pub = rospy.Publisher('/move_base/cancel', GoalID, queue_size=1)
        TelGoal_ = GoalID()
        TelGoal_.id = ''
         
        thresh = rospy.get_param('~thresh',3)
        rospy.loginfo('TO SKIP = %s ', (CurrentGoal))
        dev = list(range(1, thresh+1))
        rospy.loginfo('###############################')
        rospy.loginfo('##### REACHED SKIP #####')
        rospy.loginfo('###############################')
        under_thresh = True
        old_clear = dev[0]
        attempts = 0
        while under_thresh:
            
            try:
               clearances = rospy.wait_for_message(clear_topic, Int64, timeout=1) 
               current_pose = rospy.wait_for_message(stop_topic, PoseWithCovarianceStamped, timeout=1)
               teleop = rospy.wait_for_message('/teleop', Bool, timeout=1)
               object_moved = rospy.wait_for_message('/hrz_triggered', Bool, timeout=1)
               get_coffee = rospy.wait_for_message('/i_want_coffee', Bool, timeout=1)
               found_bed = rospy.wait_for_message('/i_found_a_bed', Bool, timeout=1)
               found_human = rospy.wait_for_message('/i_saw_a_human', Bool, timeout=1)
            except rospy.ROSException as e:
                if 'timeout exceeded' in e.message:
                    continue  # no new waypoint within timeout, looping...
                else:
                    raise e
            if clearances.data >= thresh:
                next_waypoint_index = current_index

                if current_index == len(waypoints.poses) -1:
                    next_waypoint_index = 0
                else:
                    next_waypoint_index = current_index + 1
                # userdata.next_waypoint_index = next_waypoint_index
                rospy.loginfo('Skip waypoint! Next is : %s', (next_waypoint_index))
                DataLogging('Waypoint ' + str(current_index) + ' skipped', self.data_publisher)
                PublishWarning(1, 40, 'Waypoint skipped ', 'Waypoint skipped', self.audioWarn_publisher)
                rospy.sleep(3.)
                self.start_over = True 
                return 'success'
            if teleop.data == True:
                pub.publish(TelGoal_)
                return 'teleop'
            elif object_moved.data == True:
                pub.publish(TelGoal_)
                return 'object_moved'
            elif get_coffee.data == True:
                pub.publish(TelGoal_)
                return 'get_coffee'
            elif found_bed.data == True:
                pub.publish(TelGoal_)
                return 'bed_found'
            elif found_human.data == True:
                pub.publish(TelGoal_)
                return 'human_found'
            else:
                # rospy.loginfo('Clearances : %s', (clearances.data))
                if  clearances.data==old_clear: 
                    rospy.loginfo("RESEND")

                    # self.client.send_goal_and_wait(current_goal, rospy.Duration(5))
                    self.client.send_goal(CurrentGoal)

                    if self.client.get_state() == 0:
                        rospy.loginfo("STILL BLOCKED")
                        attempts = attempts +1
                           
                    else:
                        rospy.loginfo("STILL BLOCKED NOT 0")
                        attempts = attempts +1

                    old_clear=dev[clearances.data]   


                if euclidean_distance(current_pose,CurrentGoal)<=0.1:
                        rospy.loginfo("MOVE")
                        next_waypoint_index = current_index
                        rospy.loginfo('Skip waypoint! Next is : %s', (next_waypoint_index))
                        rospy.sleep(3.)
                        self.start_over = True
                        return 'success'
                        
            rospy.loginfo('Clearances : %s', (clearances.data))
            rospy.loginfo('Old : %s', (old_clear))
            rospy.loginfo('Attempts : %s', (attempts))     
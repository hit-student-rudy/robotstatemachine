#!/usr/bin/env python

import threading
import rospy
import actionlib

from smach import State
from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseArray, PoseStamped
from std_msgs.msg import Empty, Int64, Bool, Header
from actionlib_msgs.msg import GoalID

# from Setup_Variables.variables import *

from waypoints_msgs.msg import Waypoints
from warning_message.msg import warning


class FollowPath(State): # mpainei to next vgainei to last

    
    def __init__(self, stop_):
        State.__init__(self, outcomes=['success','start_over','object_moved','out'], input_keys=['FP_waypoints_in','next_waypoint_index'],output_keys=['FP_waypoints_out','last_waypoint_index','cancelled_goal'])
        
        # setup_task_variables(self)
        
        self.frame_id = rospy.get_param('~goal_frame_id','map')
        # Get a move_base action client 
        self.client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
        rospy.loginfo('Connecting to move_base...')
        self.client.wait_for_server()
        rospy.loginfo('Connected to move_base.')
        self.publishArray_publisher = rospy.Publisher('/published_waypoints', PoseArray, queue_size=1)
        self.stopSignal_publisher = rospy.Publisher('/start_cls', Bool, queue_size=1)
        self.audioWarn_publisher = WarningsPublisher()
        self.data_publisher = DataPublisher()

        self.stop_ = stop_

        self.Skip_publisher = rospy.Publisher('/Goal_to_skip', MoveBaseGoal, latch = True, queue_size=1)
        
        
    def execute(self, userdata):
        
        rospy.loginfo('STOP IN FOLLOW IN: %s' ,(self.stop_))
        start_cls = Bool()
        
    
        # Execute waypoints each in sequence
        
        self.stop_patrol = False
        
        waypoints = PoseArray()
        waypoints.header=userdata.FP_waypoints_in.header
        waypoints.poses =userdata.FP_waypoints_in.poses  
        
        # Start thread to listen for when the path is ready (this function will end then)
        def stop_patrolling():
            """thread worker function"""
            data = rospy.wait_for_message('/stop_patrolling', Empty)
            rospy.loginfo('Recieved STOP message')
            self.stop_patrol = True
        stop_thread = threading.Thread(target=stop_patrolling)
        stop_thread.start()
        
        
        last_waypoint_index = userdata.next_waypoint_index
        rospy.loginfo('Entry index: %s ',(last_waypoint_index))

        if last_waypoint_index==[]:
           last_waypoint_index =0
        elif (last_waypoint_index == len(waypoints.poses)-1) & self.stop_ == True:
             last_waypoint_index = len(waypoints.poses)-1
        elif (last_waypoint_index == len(waypoints.poses)-1):
             last_waypoint_index = 0
        else:
            pass
        
        index = last_waypoint_index

        rospy.loginfo('Start from waypoint: %s ',(index))
        self.stop_ = False
        while not self.stop_patrol:     
            try:
           
                for waypoint in waypoints.poses[index::]:
                    
                    # break
                   
                   start_cls.data = False
                  
                   if self.stop_patrol:
                       return 'success'
                   # Break if preempted
                   if waypoints == []:
                       rospy.loginfo('The waypoint queue has been reset.')
                       break

                   # Otherwise publish next waypoint as goal
                   
                   goal = MoveBaseGoal()
                   goal.target_pose.header.frame_id = self.frame_id
                   goal.target_pose.pose.position = waypoint.position
                   goal.target_pose.pose.orientation = waypoint.orientation

                   self.client.send_goal(goal)
                   self.client.wait_for_result()
                   cancelled_index = waypoints.poses.index(waypoint)
                   rospy.loginfo("THE LAST_WAYPOINT_INDEX (Waypoint) IS: %s", (waypoints.poses.index(waypoint)))
                   rospy.loginfo("State: %s", (self.client.get_state()))
                   rospy.sleep(1.0)
                #####  Examine possible states ####
                   if self.client.get_state() == 3:
                       self.stop_ = False
                       last_waypoint_index = waypoints.poses.index(waypoint) 
                    #    rospy.loginfo('Waypoint Reached!')
                       PublishWarning(1, 40, 'Waypoint Reached!', 'Waypoint Reached!', self.audioWarn_publisher)
                       DataLogging('Waypoint '+str(last_waypoint_index)+ ' Reached', self.data_publisher)
                       self.stopSignal_publisher.publish(start_cls)
                   elif self.client.get_state() == 2: 
                       self.stop_ = True
                       start_cls.data = True
                       self.stopSignal_publisher.publish(start_cls)
                       rospy.loginfo('Cancelled index: %s',(cancelled_index))               
                       userdata.cancelled_goal = cancelled_index
                       self.Skip_publisher.publish(goal)
                       return 'out'
                   else:
                       self.stop_ = False
                       last_waypoint_index = waypoints.poses.index(waypoint)
                       rospy.loginfo('Waypoint....!')
                       self.stopSignal_publisher.publish(start_cls)
                rospy.loginfo('START_OVER , LAST IS: %s' ,(last_waypoint_index))
                userdata.last_waypoint_index = last_waypoint_index
                userdata.cancelled_goal = last_waypoint_index   
                rospy.sleep(3.)
                return 'start_over'   
            except rospy.ROSException as e:
                if 'timeout exceeded' in e.message:
                    continue  # no new waypoint within timeout, looping...
                else:
                    raise e
        return 'success'
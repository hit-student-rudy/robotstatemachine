#!/usr/bin/env python

import threading
import rospy
import actionlib

from smach import State
from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseArray, PoseStamped
from std_msgs.msg import Empty, Int64, Bool, Header
from actionlib_msgs.msg import GoalID
from visualization_msgs.msg import Marker

from waypoints_msgs.msg import Waypoints
from warning_message.msg import warning

from std_srvs.srv import Empty, EmptyRequest

from TTSMessages import TTSPublisher

# Import LiftRailing motions
from LiftRailing import LiftRailing


class AutoLiftRailing(State): 

    def __init__(self):
        State.__init__(self, outcomes=['success'])



    def execute(self, userdata):
        Lift_Railing = LiftRailing.LiftRailing()
        rospy.loginfo("Lifting Railing...")

        thread = threading.Thread(target=Lift_Railing.PerformLiftBedRailing())
        thread.start()
        rospy.loginfo("Bed processed! Back to patrol...")
        return 'success' 

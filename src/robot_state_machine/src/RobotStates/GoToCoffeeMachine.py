#!/usr/bin/env python

import threading
import rospy
import actionlib

from smach import State
from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseArray, PoseStamped
from std_msgs.msg import Empty, Int64, Bool, Header
from actionlib_msgs.msg import GoalID
from visualization_msgs.msg import Marker

from waypoints_msgs.msg import Waypoints
from warning_message.msg import warning

from std_srvs.srv import Empty, EmptyRequest

from TTSMessages.TTSPublisher import TTSPublisher

class GoToCoffeeMachine(State): 

    def __init__(self):
        State.__init__(self, outcomes=['success', 'need_help','out'])

        self.data_publisher = DataPublisher()
        self.stopSignal_publisher = rospy.Publisher('/start_cls', Bool, queue_size=1)
        self.start_cls=Bool()
        
        self.frame_id = rospy.get_param('~goal_frame_id','map')
        # Get a move_base action client 
        self.client = actionlib.SimpleActionClient('move_base', MoveBaseAction)

        self.CoffeeMachine_pub_ = rospy.Publisher('/Coffee_Machine_Label', Marker, queue_size=1)
        self.CoffeeMachineShape_pub_ = rospy.Publisher('/Coffee_Machine_Point', Marker, queue_size=1)

        self.CoffeeMachineTeleop_pub_ = rospy.Publisher('/Coffee_teleop', Bool, queue_size=1)

        rospy.wait_for_service('/move_base/clear_costmaps')
        self.clear_costmaps_srv = rospy.ServiceProxy('/move_base/clear_costmaps', Empty)
        
        self.coffee_publisher = TTSPublisher()
        


    def execute(self, userdata):
        rospy.loginfo("Entered GoToCoffeeMachine State")
        DataLogging('Entered GoToCoffeeMachine State', self.data_publisher)

        self.coffee_publisher.PublishWarning("Wilt u Koffie? Ik ga koffie voor u halen!")
        
        coffee_topic = '/i_want_coffee'
        hrz_topic = '/hrz_triggered'
        go_to_coffee_machine = True
        self.start_cls.data = False

        teleop = Bool()
        teleop.data = True
        
        coffee_machine_pose = PoseWithCovarianceStamped()
        coffee_machine_pose.header.frame_id = self.frame_id
        coffee_machine_pose.header.stamp = rospy.Time.now()
        coffee_machine_pose.pose.pose.position.x =  -4.3  #0.06 # 1
        coffee_machine_pose.pose.pose.position.y =   7.7  #1.05 # 
        coffee_machine_pose.pose.pose.position.z = 0.0   
        coffee_machine_pose.pose.pose.orientation.x =  0.0
        coffee_machine_pose.pose.pose.orientation.y =  0.0
        coffee_machine_pose.pose.pose.orientation.z = -0.56
        coffee_machine_pose.pose.pose.orientation.w =  0.82     
        coffee_machine = Marker()
        coffee_machine.header.frame_id = coffee_machine_pose.header.frame_id
        coffee_machine.header.stamp = rospy.Time.now()
        coffee_machine.ns = 'coffee_machine'
        coffee_machine.id = 0
        coffee_machine.type = coffee_machine.TEXT_VIEW_FACING
        coffee_machine.action = coffee_machine.ADD
        coffee_machine.pose.position.x = coffee_machine_pose.pose.pose.position.x
        coffee_machine.pose.position.y = coffee_machine_pose.pose.pose.position.y
        coffee_machine.pose.position.z = 0.5
        coffee_machine.pose.orientation.w= 1.0
        coffee_machine.scale.x = 0.6
        coffee_machine.scale.y = 0.6
        coffee_machine.scale.z = 0.6
        coffee_machine.color.a = 1.0
        coffee_machine.color.r = 0.2
        coffee_machine.color.g = 0.7
        coffee_machine.color.b = 0.0
        coffee_machine.text = "Coffee_Machine"

        coffee_machine_shape = Marker()
        coffee_machine_shape.header.frame_id = coffee_machine_pose.header.frame_id
        coffee_machine_shape.header.stamp = rospy.Time.now()
        coffee_machine_shape.ns = 'coffee_machine'
        coffee_machine_shape.id = 1
        coffee_machine_shape.type = coffee_machine.SPHERE
        coffee_machine_shape.action = coffee_machine.ADD
        coffee_machine_shape.pose.position.x = coffee_machine_pose.pose.pose.position.x
        coffee_machine_shape.pose.position.y = coffee_machine_pose.pose.pose.position.y
        coffee_machine_shape.pose.position.z = 0.0
        coffee_machine_shape.pose.orientation.w= 1.0
        coffee_machine_shape.scale.x = 0.5
        coffee_machine_shape.scale.y = 0.5
        coffee_machine_shape.scale.z = 0.5
        coffee_machine_shape.color.a = 1.0
        coffee_machine_shape.color.r = 0.2
        coffee_machine_shape.color.g = 0.7
        coffee_machine_shape.color.b = 0.0

        self.CoffeeMachine_pub_.publish(coffee_machine)
        self.CoffeeMachineShape_pub_.publish(coffee_machine_shape)
        self.stopSignal_publisher.publish(self.start_cls)

        attempts = 0

        while go_to_coffee_machine:

            try:
                active = rospy.wait_for_message(coffee_topic, Bool, timeout=1)
                rospy.loginfo('COFFEE: %s',(active.data))
            except rospy.ROSException as e:
                if 'timeout exceeded' in e.message:
                    continue  # no new waypoint within timeout, looping...
                else:
                    raise e

            goal = MoveBaseGoal()
            goal.target_pose.header.frame_id = self.frame_id
            goal.target_pose.pose = coffee_machine_pose.pose.pose
            
            rospy.loginfo("ATTEMPTS: %s", (attempts) )

            if attempts >= 3:
                self.coffee_publisher.PublishWarning("Oeps, Ik kan niet naar de Koffie Machine gaan, Kunt u mee helpen?")
                rospy.sleep(3.0)
                return 'need_help'
            else:
                pass

            self.client.send_goal(goal)
            
            self.client.wait_for_result()
            print(self.client.get_state())
            if self.client.get_state() == 3: 
                # PublishWarning(1, 40, 'Coffee machine!', 'Coffee machine!', self.audioWarn_publisher)
                # DataLogging('Coffee machine!', self.data_publisher)
                go_to_coffee_machine = False
                self.CoffeeMachineTeleop_pub_.publish(teleop)
                rospy.loginfo("EXIT HERE")
                return 'success'
            elif self.client.get_state() == 2:
                hrz = rospy.wait_for_message(hrz_topic, Bool, timeout=1)
                if (hrz.data):
                    attempts = 0
                    return 'out' 
                else:
                    pass
            
                rospy.loginfo('Coffe Machine looks occupied. I ll attempt again!')
        
                self.clear_costmaps_srv(EmptyRequest())
                rospy.sleep(3.0)
                attempts = attempts +1
            elif self.client.get_state() == 4: 
                rospy.loginfo('Coffe Machine looks occupied. I ll attempt again!')
                self.clear_costmaps_srv(EmptyRequest())
                rospy.sleep(3.0)
                attempts = attempts +1
            else:
                attempts = 0
                if active.data == False:
                    go_to_coffee_machine = False
                    self.CoffeeMachineTeleop_pub_.publish(teleop)
                else:
                    go_to_coffee_machine = True
                   
        rospy.loginfo("Exiting GoToCoffeeMachine State..")
        rospy.sleep(2.0)
        rospy.loginfo("FINISHED")         
        return 'success'

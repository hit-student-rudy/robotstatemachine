import rospy
import numpy as np

import cv2
from cv_bridge import CvBridge, CvBridgeError

from sensor_msgs.msg import Image, CameraInfo

class faceDetector(object):

    def __init__(self):
        #find path to face xml files and change the location below
        self.faceCascPath = "/opt/ros/indigo/share/OpenCV-3.1.0-dev/haarcascades/haarcascade_frontalface_default.xml"
        self.face2CascPath = "/opt/ros/indigo/share/OpenCV-3.1.0-dev/haarcascades/haarcascade_frontalface_alt.xml"
        self.rgbRawImageTopic = '/xtion/rgb/image_raw' #rospy.get_param("rgbRawImageTopic",'/xtion/rgb/image_raw')
        self.cameraInfoTopic = '/xtion/rgb/camera_info' #rospy.get_param("cameraInfoTopic",'/xtion/rgb/camera_info')
        self.faceCascade = cv2.CascadeClassifier(self.faceCascPath)
        self.face2Cascade = cv2.CascadeClassifier(self.face2CascPath)
        self.bridge = CvBridge()
        dt = np.dtype('Float64')
        self.cameraIntrinsics = np.zeros( (3, 3), dtype=dt)
        self.raw_image_subscriber = rospy.Subscriber(self.rgbRawImageTopic , Image, self.raw_image_callback)
        self.camera_info_subscriber = rospy.Subscriber(self.cameraInfoTopic, CameraInfo, self.intrinsic_parameters_callback)
        self.gray = None 

    def detector(self):
        faces = self.faceCascade.detectMultiScale(
            self.gray,
            scaleFactor=1.4,
            minNeighbors=6,
            minSize=(35, 35),
            flags=cv2.CASCADE_SCALE_IMAGE
        )
        faces2 = self.face2Cascade.detectMultiScale(
            self.gray,
            scaleFactor=1.4,
            minNeighbors=6,
            minSize=(35, 35),
            flags=cv2.CASCADE_SCALE_IMAGE
        )
        for (x, y, w, h) in faces:
            cv2.rectangle(self.cv_image, (x, y), (x+w, y+h), (0, 255, 0), 2)
        for (x, y, w, h) in faces2:
            cv2.rectangle(self.cv_image, (x, y), (x+w, y+h), (0, 255, 0), 2)
     
        if len(faces) > 0 and len(faces2) > 0:
            combinedFaces = np.append(faces, faces2, axis=0)
            combinedFaces = combinedFaces.tolist()
            filteredFaces, weights = cv2.groupRectangles(
                combinedFaces,
                groupThreshold = 1,
                eps = 0.85
                )
            filteredFaces = np.asarray(filteredFaces)
            area = 0.1
            i = -1
            id = 0
            for (x, y, w, h) in filteredFaces:
                cv2.rectangle(self.originalImage, (x, y), (x+w, y+h), (255, 0, 0), 5)
                i += 1
                if w*h > area:
                    area = w*h
                    id = i


            to_look_x = (filteredFaces[id][0]+filteredFaces[id][2]/2 - self.cameraIntrinsics[0][2])/ self.cameraIntrinsics[0][0]
            to_look_y = (filteredFaces[id][1]+filteredFaces[id][3]/2 - self.cameraIntrinsics[1][2])/ self.cameraIntrinsics[1][1]

               
            # cv2.imshow('Face Detection',self.originalImage)
            # cv2.waitKey(0.1)

            return to_look_x, to_look_y
        
        else:
            print("ERROR")

    def intrinsic_parameters_callback(self, camera_params):
        self.cameraIntrinsics[0,0] = camera_params.K[0]
        self.cameraIntrinsics[1,1] = camera_params.K[4]
        self.cameraIntrinsics[0,2] = camera_params.K[2]
        self.cameraIntrinsics[1,2] = camera_params.K[5]
        self.cameraIntrinsics[2,2] = 1.0

    def raw_image_callback(self, raw_image):
        self.cv_image = self.bridge.imgmsg_to_cv2(raw_image, desired_encoding="bgr8")
        self.originalImage = self.cv_image.copy()
        self.gray = cv2.cvtColor(self.cv_image, cv2.COLOR_BGR2GRAY)
#!/usr/bin/env python

import rospy
from control_msgs.msg import PointHeadActionGoal
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from std_msgs.msg import Empty, Int64, Bool, Header

from .face_detector import faceDetector

class faceTracker(object):
    def __init__(self):
        self.rgbOpticaFrameTopic = '/xtion_rgb_optical_frame' #rospy.get_param("rgbOpticaFrameTopic", '/xtion_rgb_optical_frame')
        self.headJoint1 = 'head_1_joint' #rospy.get_param("headJoint1",  'head_1_joint')
        self.headJoint2 = 'head_2_joint' #rospy.get_param("headJoint2",  'head_2_joint')
        self.headControllerCommand = '/head_controller/command' #rospy.get_param("headControllerCommand",  '/head_controller/command')
        self.headControllerActionGoal = 'head_controller/point_head_action/goal' #rospy.get_param("headControllerActionGoal",  'head_controller/point_head_action/goal')
        self.cenPub = rospy.Publisher(self.headControllerCommand, JointTrajectory, queue_size=1)
        self.pub = rospy.Publisher(self.headControllerActionGoal, PointHeadActionGoal, queue_size=1)
        self.NoFaceCountPub = rospy.Publisher('/no_face_counter', Int64, queue_size=1)
        self.detect = faceDetector()
        self.counter = 0

    def tracker(self):
        try:
            rospy.loginfo("START DETECTOR")
            # count = Int64()
            x, y = self.detect.detector()
            # print("X : %f || Y : %f ",to_look_x,to_look_y)
            distance_z = 1.0
            head_move = PointHeadActionGoal()
            head_move.goal.target.header.frame_id = self.rgbOpticaFrameTopic
            head_move.goal.pointing_frame = self.rgbOpticaFrameTopic
            head_move.goal.pointing_axis.z = 1.0
            head_move.goal.target.point.x= x * distance_z
            head_move.goal.target.point.y= y * distance_z 
            head_move.goal.target.point.z= distance_z
            head_move.goal.min_duration = rospy.Duration(0.25)
            head_move.goal.max_velocity = 1.0
            # self.counter = 0
            # count.data = self.counter
            # self.NoFaceCountPub.publish(count)
            self.pub.publish(head_move)

        except:
            rospy.loginfo("INTO EXCEPTION")
            # count = Int64()
            # self.counter = self.counter +1 
            # count.data = self.counter
            head_move = JointTrajectory()
            pos = JointTrajectoryPoint()
            head_move.joint_names = [self.headJoint1, self.headJoint2]
            pos.positions = [0.0,0.3]
            pos.time_from_start = rospy.Duration.from_sec(2.0)
            head_move.points.append(pos)
            # self.NoFaceCountPub.publish(count)
            self.cenPub.publish(head_move)
#!/usr/bin/env python3

import threading
import rospy
import actionlib

from smach import State
from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseArray, PoseStamped
from std_msgs.msg import Empty, Int64, Bool, Header
from actionlib_msgs.msg import GoalID
from visualization_msgs.msg import Marker

from waypoints_msgs.msg import Waypoints
from warning_message.msg import warning

from std_srvs.srv import Empty, EmptyRequest

from TTSMessages.TTSPublisher import TTSPublisher

from .FaceRecognitionModules.face_detector import faceDetector
from .FaceRecognitionModules.face_tracker import faceTracker

# Import LiftRailing motions
from LiftRailing import LiftRailing

class FaceRecognition(State): 

    def __init__(self):
        State.__init__(self, outcomes=['success','abort', 'need_help'])

        self.data_publisher = DataPublisher()
        self.stopSignal_publisher = rospy.Publisher('/start_cls', Bool, queue_size=1)
        self.start_cls=Bool()
        
        self.frame_id = rospy.get_param('~goal_frame_id','map')

        self.faceRec_publisher = TTSPublisher()

        self.Controllers = LiftRailing.LiftRailing()
        

    def execute(self, userdata):
        rospy.loginfo("Entered FaceRecognition State")
        DataLogging('Entered FaceRecognition State', self.data_publisher)

        # self.faceRec_publisher.PublishWarning("Hello. Have you lost your way?!")
        
        human_topic = '/i_saw_a_human'
        # hrz_topic = '/hrz_triggered'
        # stop_interact_topic = '/end_of_dialog'
        teleop_topic = 'teleop'

        keep_alive = True
        self.start_cls.data = False

        # teleop = Bool()
        # teleop.data = True
        # 
        self.stopSignal_publisher.publish(self.start_cls)
# 
        try:
            tracker = faceTracker()
        except:
            rospy.logerr("Something wrong with face module")
            return 'abort'

        while keep_alive:
# 
            try:
                active = rospy.wait_for_message(human_topic, Bool, timeout=1)
                # hrz = rospy.wait_for_message(hrz_topic, Bool, timeout=1)
                # stop_interact = rospy.wait_for_message(stop_interact_topic, Bool, timeout=1)
                teleop = rospy.wait_for_message(teleop_topic, Bool, timeout=1)
                # 
                rospy.loginfo('INTERACT: %s',(active.data))
            except rospy.ROSException as e:
                if 'timeout exceeded' in e.message:
                    continue  # no new waypoint within timeout, looping...
                else:
                    raise e
            # 
            tracker.tracker()
            rospy.loginfo('TRACKS')
            # if stop_interact.data:
            #     keep_alive = False
# 
            if active.data == False:
                rospy.loginfo("Exiting FaceRecognition State..")
                self.Controllers.ReachNavigationConfig()
                rospy.sleep(2.0)
                rospy.loginfo("FINISHED")    
                return 'success'
            elif teleop.data == True:
                rospy.loginfo("Exiting FaceRecognition State..")
                rospy.loginfo("NEED HELP")  
                rospy.sleep(2.0)
                return 'need_help'
            else:
                pass
            rospy.sleep(0.1)       
        rospy.loginfo("Exiting FaceRecognition State..")
        self.Controllers.ReachNavigationConfig()
        rospy.sleep(2.0)
        rospy.loginfo("FINISHED")         
        return 'success'

#!/usr/bin/env python3

import threading
import rospy
import actionlib

from smach import State
from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseArray, PoseStamped
from std_msgs.msg import Empty, Int64, Bool, Header
from actionlib_msgs.msg import GoalID
from visualization_msgs.msg import Marker

from waypoints_msgs.msg import Waypoints
from warning_message.msg import warning

from std_srvs.srv import Empty, EmptyRequest

from TTSMessages.TTSPublisher import TTSPublisher

from .HumanApproachModules.positionApproacher import approacher

# Import LiftRailing motions
# from LiftRailing import LiftRailing

class HumanApproach(State): 

    def __init__(self):
        State.__init__(self, outcomes=['success','out','need_help','abort'])

        self.data_publisher = DataPublisher()
        self.stopSignal_publisher = rospy.Publisher('/start_cls', Bool, queue_size=1)
        self.start_cls=Bool()
        
        self.frame_id = rospy.get_param('~goal_frame_id','map')
        # Get a move_base action client 
        self.client = actionlib.SimpleActionClient('move_base', MoveBaseAction)

        self.Human_pub_ = rospy.Publisher('/human_Label', Marker, queue_size=1)
        self.HumanShape_pub_ = rospy.Publisher('/human_Point', Marker, queue_size=1)

        rospy.wait_for_service('/move_base/clear_costmaps')
        self.clear_costmaps_srv = rospy.ServiceProxy('/move_base/clear_costmaps', Empty)
        
        self.humanApproach_publisher = TTSPublisher()
        # self.Controllers = LiftRailing.LiftRailing()
        

    def execute(self, userdata):
        rospy.loginfo("Entered HumanApproach State")
        DataLogging('Entered HumanApproach State', self.data_publisher)
        # self.Controllers.start_controllers()
        # self.humanApproach_publisher.PublishWarning("Ohh I see someone in the hallway!")
        
        human_topic = '/i_saw_a_human'
        hrz_topic = '/hrz_triggered'
        teleop_topic = '/teleop'

        go_to_human = True
        self.start_cls.data = False

        # human_pose = PoseWithCovarianceStamped()
        # human_pose.header.frame_id = self.frame_id
        # human_pose.header.stamp = rospy.Time.now()
        # human_pose.pose.pose.position.x =  -4.3  #0.06 # 1
        # human_pose.pose.pose.position.y =   7.7  #1.05 # 
        # human_pose.pose.pose.position.z = 0.0   
        # human_pose.pose.pose.orientation.x =  0.0
        # human_pose.pose.pose.orientation.y =  0.0
        # human_pose.pose.pose.orientation.z = -0.56
        # human_pose.pose.pose.orientation.w =  0.82     
        # human = Marker()
        # human.header.frame_id = human_pose.header.frame_id
        # human.header.stamp = rospy.Time.now()
        # human.ns = 'human'
        # human.id = 0
        # human.type = human.TEXT_VIEW_FACING
        # human.action = human.ADD
        # human.pose.position.x = human.pose.pose.position.x
        # human.pose.position.y = human_pose.pose.pose.position.y
        # human.pose.position.z = 0.5
        # human.pose.orientation.w= 1.0
        # human.scale.x = 0.6
        # human.scale.y = 0.6
        # human.scale.z = 0.6
        # human.color.a = 1.0
        # human.color.r = 0.0
        # human.color.g = 1.0
        # human.color.b = 1.0
        # human.text = "Human"

        # human_shape = Marker()
        # human_shape.header.frame_id = human_pose.header.frame_id
        # human_shape.header.stamp = rospy.Time.now()
        # human_shape.ns = 'human'
        # human_shape.id = 1
        # human_shape.type = human.SPHERE
        # human_shape.action = human.ADD
        # human_shape.pose.position.x = human_pose.pose.pose.position.x
        # human_shape.pose.position.y = human_pose.pose.pose.position.y
        # human_shape.pose.position.z = 0.0
        # human_shape.pose.orientation.w= 1.0
        # human_shape.scale.x = 0.5
        # human_shape.scale.y = 0.5
        # human_shape.scale.z = 0.5
        # human_shape.color.a = 1.0
        # human_shape.color.r = 0.0
        # human_shape.color.g = 1.0
        # human_shape.color.b = 1.0

        # self.Human_pub_.publish(human)
        # self.HumanShape_pub_.publish(human_shape)
        self.stopSignal_publisher.publish(self.start_cls)

        # pub = rospy.Publisher('/move_base/cancel', GoalID, queue_size=1)
        # TelGoal_ = GoalID()
        # TelGoal_.id = ''

        try:
            approach = approacher()
        except:
            rospy.logerr("Something Wrong with Approaching Module")
            rospy.signal_shutdown("Something Wrong with Approaching Module")

        # attempts = 0

        while go_to_human:

            try:
                active = rospy.wait_for_message(human_topic, Bool, timeout=1)
                hrz = rospy.wait_for_message(hrz_topic, Bool, timeout=1)
                teleop = rospy.wait_for_message(teleop_topic, Bool, timeout=1)
                rospy.loginfo('APPROACH: %s',(active.data))
            except rospy.ROSException as e:
                if 'timeout exceeded' in e.message:
                    continue  # no new waypoint within timeout, looping...
                else:
                    raise e

            status = approach.beginApproach()

            # if hrz.data == True:
            #     pub.publish(TelGoal_)
            #     return 'out'
            # elif active.data == False:
            #     pub.publish(TelGoal_)
            #     return 'abort'
            # elif teleop.data == True:
            #     pub.publish(TelGoal_)
            #     return 'need_help'
            # else:
            #     pass

            if status == 0:
                print("Can't Approach anyone, go back for patroling")
                rospy.sleep(2)
                # rospy.signal_shutdown("Can't Approach anyone, go back for patroling")
                return 'abort'
            elif status == 100:
                return 'out'
            elif status == 1:
                print("Reached The Closest Person, Start Other Modules")
                rospy.sleep(2)
                # rospy.signal_shutdown("Reached The Closest Person, Start Other Modules")
                return 'success'
            elif status == 2:
                pass

            # goal = MoveBaseGoal()
            # goal.target_pose.header.frame_id = self.frame_id
            # goal.target_pose.pose = human_pose.pose.pose
            
            # rospy.loginfo("ATTEMPTS: %s", (attempts) )

            # if attempts >= 3:
            #     self.coffee_publisher.PublishWarning("Oeps, Ik kan niet naar de Persoon gaan, Kunt u mee helpen?")
            #     rospy.sleep(3.0)
            #     return 'need_help'
            # else:
            #     pass

            # self.client.send_goal(goal)
            
            # self.client.wait_for_result()
            # print(self.client.get_state())
            # if self.client.get_state() == 3: 
            #     # PublishWarning(1, 40, 'Coffee machine!', 'Coffee machine!', self.audioWarn_publisher)
            #     # DataLogging('Coffee machine!', self.data_publisher)
            #     go_to_human = False

            #     rospy.loginfo("EXIT HERE")
            #     return 'success'
            # elif self.client.get_state() == 2:
            #     hrz = rospy.wait_for_message(hrz_topic, Bool, timeout=1)
            #     if (hrz.data):
            #         attempts = 0
            #         return 'out' 
            #     else:
            #         pass
            #     # while hrz.data:
            #         # rospy.loginfo("Something moved close to me")
            #         # rospy.sleep(0.1)
            #     rospy.loginfo('Coffe Machine looks occupied. I ll attempt again!')
                
            #     self.clear_costmaps_srv(EmptyRequest())
            #     rospy.sleep(3.0)
            #     attempts = attempts +1
            # elif self.client.get_state() == 4: 
            #     rospy.loginfo('Coffe Machine looks occupied. I ll attempt again!')
            #     self.clear_costmaps_srv(EmptyRequest())
            #     rospy.sleep(3.0)
            #     attempts = attempts +1
            # else:
            #     attempts = 0
            #     if active.data == False:
            #         go_to_human = False
            #         self.CoffeeMachineTeleop_pub_.publish(teleop)
            #     else:
            #         go_to_human = True
                   
        rospy.loginfo("Exiting HumanApproach State..")
        rospy.sleep(2.0)
        rospy.loginfo("FINISHED")         
        return 'success'

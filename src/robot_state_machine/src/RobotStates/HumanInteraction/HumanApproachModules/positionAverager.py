#!/usr/bin/env python

import rospy

from std_msgs.msg import Bool
from people_msgs.msg import People, Person
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseWithCovarianceStamped

import math

from sklearn.cluster import KMeans

class positionAverager(object): # object for this class is created in positionApproacher.py

    def __init__(self):
        self.robot_pose = Pose()
        self.people_positions = People()
        self.person_position = Person()
        self.countFrames = 0
        self.peopleCoordinates = []
        self.updatedPeopleLocations = []
        self.flag = False # used to make sure whether position data is updated or not

        # change these in launch file
        self.robotPoseTopic = '/amcl_pose' #rospy.get_param("robotPoseTopic", '/amcl_pose')
        self.peopleTopic = '/people' #rospy.get_param("peopleTopic", '/people')
        self.maxDistance = 10.0 #rospy.get_param("maxDistance", '10.0')

        self.robot_position_subscriber = rospy.Subscriber(self.robotPoseTopic, PoseWithCovarianceStamped, self.robot_position_callback)
        self.human_detector_subscriber = rospy.Subscriber(self.peopleTopic, People, self.human_detector_callback)

    def human_detector_callback(self, peoplePositions):
        for i in peoplePositions.people:
            self.peopleCoordinates.append([i.position.x, i.position.y])
            self.countFrames += 1

        if self.countFrames >= 10:
            kmeans = KMeans(n_clusters=len(peoplePositions.people)).fit(self.peopleCoordinates)
            self.updatedPeopleLocations = kmeans.cluster_centers_
            self.peopleCoordinates = []
            self.countFrames = 0
            self.flag = True
        
    def robot_position_callback(self, robotPosition):
        self.robot_pose = robotPosition.pose.pose

    def get_ordered_list(self, updatedPeopleLocations):
        dis = []
        for i in updatedPeopleLocations:
            dis.append(math.sqrt(math.pow((self.robot_pose.position.x - i[0]), 2) + math.pow((self.robot_pose.position.y - i[1]), 2)))
    
        zipped = list(zip(dis, updatedPeopleLocations))
        zipped.sort()
        dis, orderedLocations =  zip(*zipped)
        return orderedLocations

    def getPosition(self):
        while self.flag != True:
            pass
    
        self.flag = False
        return self.get_ordered_list(self.updatedPeopleLocations), self.robot_pose

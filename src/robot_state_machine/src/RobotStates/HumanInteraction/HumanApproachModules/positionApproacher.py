#!/usr/bin/env python

import rospy
import actionlib
# from tf.transformations import quaternion_from_euler

from pyquaternion import Quaternion as Quat

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from people_msgs.msg import People
from geometry_msgs.msg import PoseStamped, Quaternion, Pose
from visualization_msgs.msg import Marker, MarkerArray
from actionlib_msgs.msg import GoalID
from std_msgs.msg import Empty, Int64, Bool, Header
import math


import os
import sys

from .positionAverager import positionAverager

class approacher(object):
    def __init__(self):
        self.averager = positionAverager() # object created for positionAverager
        self.peoplePositions = None
        self.robot_pose = None
        self.status = None #used for providing update on what the module is currently doing
                            # 0 is for going back to patroling
                            # 1 is reached near the desired person
                            # 2 is running module again to reach now/updated person location
        
        # getting required input from launch files
        self.markerDisplayTopic = rospy.get_param("markerDisplayTopic", 'visualization_marker_array')
        self.variedDistance = float(rospy.get_param("variedDis", '0.5'))
        self.distanceFromPerson = float(rospy.get_param("distanceFromPerson", '1.0'))
        self.pointsAroundPerson = int(rospy.get_param("pointsAroundPerson", '5'))
        self.displayPoints = True #rospy.get_param("displayPoints",'True')
        self.peoplTopic = rospy.get_param("peopleTopic", '/people')
        self.mapFrameID = rospy.get_param("mapFrameID",'map')
        self.move_base = rospy.get_param("move_base", 'move_base')
        
        self.markerPublisher = rospy.Publisher(self.markerDisplayTopic, MarkerArray, queue_size=1)


    # always call this function first to begin the approaching module, this returns status of the module "status" 0/1/2
    def beginApproach(self):
       

        index = 0
        value = rospy.wait_for_message(self.peoplTopic, People) # check if someone is even there or not

        if len(value.people) < 1: #if no one is found, return status 0
            return 0

        self.peoplePositions, self.robot_pose = self.averager.getPosition() # get locations 

        # if self.peoplePositions == self.robot_pose:  ## probably not needed, remove in near future
        #     return 0
        
        for personPosition in self.peoplePositions: # for every person found
            
            points = self.generateApproachingPoints(personPosition)
            
            if self.displayPoints == True:
                self.displayMarkers(points)
            
            for p in points:
                state = self.move_base_simple_client(p, personPosition) # provides feedback on action to move the base
                if state == 3:                                              # read here to know different possible feedbacks
                    # checking again whether the person has moved or not    # (http://docs.ros.org/diamondback/api/move_base_msgs/html/msg/MoveBaseAction.html)
                    self.status = self.checkAgain(personPosition, index)
                    return self.status
                if state == 2:
                   hrz = rospy.wait_for_message('/hrz_triggered' , Bool, timeout=1)
                   if (hrz.data):
                       self.status = 100
                       
                   else:
                       pass 
                   return self.status 
                elif state == 4 or state == 5:
                    pass

            index += 1 # keeps track of current person being approached
            print("Can't find path to current person, Moving to next one !")
        self.status = 0
        return self.status  # all points were checked and cant approach this person
                            # so move to the next one now and do everything again for this person as well

    def generateApproachingPoints(self, position): # generates approaching points around person 
        points = []
        for i in range(self.pointsAroundPerson):
            # creats points around person at defined distance in a circle 
            x = position[0] + self.distanceFromPerson * math.cos(math.radians(i*(360/self.pointsAroundPerson))) #simple circle equations
            y = position[1] + self.distanceFromPerson * math.sin(math.radians(i*(360/self.pointsAroundPerson))) #simple circle equations
            points.append([x, y])
            
        return self.sortPoints(points)

    def checkAgain(self, personPosition, index):
        value = rospy.wait_for_message(self.peoplTopic, People)
        if len(value.people) < 1:
            return 0
        peoplePositions, self.robot_pose = self.averager.getPosition()
        if math.sqrt(math.pow((peoplePositions[0][0] -personPosition[0]), 2) 
            + math.pow((peoplePositions[0][1] - personPosition[1]), 2)) > self.variedDistance:
            print("Approach Again")
            return 2
        else:
            return 1

    def sortPoints(self, points): # sorts points based on distance between robot and the point
        dis = []
        for i in points:
            dis.append(math.sqrt(math.pow((self.robot_pose.position.y - i[0]), 2) + math.pow((self.robot_pose.position.y - i[1]), 2)))

        zipped = zip(dis, points)
        zip_lis = list(zipped)
        zip_lis.sort()
        dis, points =  zip(*zip_lis)
        return points

    def move_base_simple_client(self, point, personPosition):
        # create a client to send goal and get feedback
        client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
        client.wait_for_server()
       
        pub = rospy.Publisher('/move_base/cancel', GoalID, queue_size=1)
        TelGoal_ = GoalID()
        TelGoal_.id = ''

        goal = MoveBaseGoal()
        goal.target_pose.header.frame_id = self.mapFrameID
        goal.target_pose.pose.position.z = 0.0
        goal.target_pose.pose.position.x = point[0]
        goal.target_pose.pose.position.y = point[1]

        # find out the right orientation of the robot towards the human
        destinationOrientation = self.findOrientation(point, personPosition)

        goal.target_pose.pose.orientation = destinationOrientation

        # send goal and wait until robot reaches the published goal
        client.send_goal_and_wait(goal)
        print("waiting ended")

        # return state of the goal(reached, aborted or can't reach due to physical limitation)
        return client.get_state()

    def findOrientation(self, point, personPosition):
        # calculate final orientation use simple trigonometry tan[(y2 - y1)/(x2 -x1)]
        delta_x = (personPosition[0] - point[0])
        delta_y = (personPosition[1] - point[1])
        angleBetween = math.atan2(delta_y, delta_x)

        # converting simple degrees to quaterion form 
        # destinationOrientation = quaternion_from_euler(0, 0, angleBetween)
        destinationOrientation = Quat(axis=[0, 0, 1], angle=angleBetween)
        return Quaternion(destinationOrientation.x, destinationOrientation.y, destinationOrientation.z, destinationOrientation.w)

    def displayMarkers(self, points): # used to display approaching points in RVIZ
        all_markers = MarkerArray()

        j = 0
        # FOR ALL POINTS
        for i in points:
            cirPos = self.defineMarker()
            cirPos.id = j
            cirPos.pose.position.x = i[0]
            cirPos.pose.position.y = i[1]
            all_markers.markers.insert(j, cirPos)
            j += 1 
        
        self.markerPublisher.publish(all_markers)

    def defineMarker(self):
        cirPos = Marker()
        cirPos.type = cirPos.SPHERE
        cirPos.header.frame_id = self.mapFrameID
        cirPos.ns = "person_circle"
        cirPos.pose.position.z = 0.0
        cirPos.pose.orientation.w = 1.0
        cirPos.scale.x = 0.2
        cirPos.scale.y = 0.2
        cirPos.scale.z = 0.2
        cirPos.color.r = 0.0
        cirPos.color.g = 1.0
        cirPos.color.b = 0.0
        cirPos.color.a = 1.0
        cirPos.action = cirPos.ADD
        # cirPos.lifetime = rospy.Duration()
        cirPos.header.stamp = rospy.Time.now()
        return cirPos 

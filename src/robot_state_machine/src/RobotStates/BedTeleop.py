#!/usr/bin/env python

import threading
import rospy
import actionlib

from smach import State
from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseArray, PoseStamped
from std_msgs.msg import Empty, Int64, Bool, Header
from actionlib_msgs.msg import GoalID

# from Setup_Variables.variables import *

from waypoints_msgs.msg import Waypoints
from warning_message.msg import warning



class BedTeleop(State): 

    def __init__(self):
        State.__init__(self, outcomes=['success'])

        self.data_publisher = DataPublisher()
        self.teleop_active = True
        self.stopSignal_publisher = rospy.Publisher('/start_cls', Bool, queue_size=1)
        self.start_cls=Bool()
        


    def execute(self, userdata):
        rospy.loginfo("Entered BedTeleoperation State")
        DataLogging('Entered BedTeleoperation State', self.data_publisher)
        
        teleop_topic = '/bed_teleop'
        self.teleop_active = True
        self.start_cls.data = False

        while self.teleop_active:
            try:
                active = rospy.wait_for_message(teleop_topic, Bool, timeout=1)
                rospy.loginfo('COFFEE_TELEOP: %s',(active.data))
            except rospy.ROSException as e:
                if 'timeout exceeded' in e.message:
                    continue  # no new waypoint within timeout, looping...
                else:
                    raise e
            if active.data == False:
                    self.teleop_active = False
            else:
                    self.teleop_active = True
            self.stopSignal_publisher.publish(self.start_cls)
        rospy.loginfo("Exiting BedTeleoperation State..")
        rospy.sleep(2.0)
        rospy.loginfo("FINISHED")         
        return 'success'

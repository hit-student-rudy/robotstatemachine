#!/usr/bin/env python

import threading
import rospy
import actionlib

from smach import State
from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from waypoints_msgs.msg import Waypoints
from warning_message.msg import warning


class PathComplete(State):
    def __init__(self):
        State.__init__(self, outcomes=['success'])
        self.data_publisher = DataPublisher()

    def execute(self, userdata):
        rospy.loginfo('###############################')
        rospy.loginfo('##### REACHED FINISH GATE #####')
        rospy.loginfo('###############################')
        return 'success'
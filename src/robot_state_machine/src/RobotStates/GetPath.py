#!/usr/bin/env python

from smach import State
from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from Setup_Variables.variables import * 

from waypoints_msgs.msg import Waypoints
from warning_message.msg import warning

from std_msgs.msg import Bool

class GetPath(State):
    def __init__(self,start_over):
        State.__init__(self, outcomes=['success'], input_keys=['waypoints_in'], output_keys=['waypoints_out'])
        
        # Create publsher to publish waypoints as pose array so that you can see them in rviz, etc.
        self.poseArray_publisher = rospy.Publisher('/waypoints', PoseArray, latch=True, queue_size=1)
        self.audioWarn_publisher = WarningsPublisher()
        self.data_publisher = DataPublisher()
        self.start_over = start_over
        self.waypoints = 0
        
        
        
        # Start thread to listen for reset messages to clear the waypoint queue
        def wait_for_path_reset():
            """thread worker function"""
            # global waypoints
            while not rospy.is_shutdown():
                data = rospy.wait_for_message('/path_reset', Empty)
                rospy.loginfo('Recieved path RESET message')
                self.initialize_path_queue()
                rospy.sleep(3) # Wait 3 seconds because `rostopic echo` latches
                               # for three seconds and wait_for_message() in a
                               # loop will see it again.
        reset_thread = threading.Thread(target=wait_for_path_reset)
        reset_thread.start()
    
    def initialize_path_queue(self):
        # global waypoints 
        waypoints = PoseArray()
        waypoints.header.frame_id = 'map'
        self.poseArray_publisher.publish(waypoints)
    
    
    def execute(self, userdata):
        
        rospy.loginfo('Start OVER: %s',(self.start_over))
        rospy.loginfo('WAYPOINTS IN: %s',(userdata.waypoints_in))

        if self.start_over == True:
            return 'success'


        self.initialize_path_queue()
        self.path_ready = False

        # Start thread to listen for when the path is ready (this function will end then)
        def wait_for_path_ready():
            """thread worker function"""
            data = rospy.wait_for_message('/path_ready', Empty)
            rospy.loginfo('Recieved path READY message')
            self.path_ready = True
        ready_thread = threading.Thread(target=wait_for_path_ready)
        ready_thread.start()

        topic = "/publish_waypoints/waypoints_list"
        rospy.loginfo("Waiting to recieve waypoints via Pose msg on topic %s" % topic)
        rospy.loginfo("To start following waypoints: 'rostopic pub /path_ready std_msgs/Empty -1'")

        # Wait for published waypoints
        while not self.path_ready:
            try:
                waypoints = rospy.wait_for_message(topic, PoseArray, timeout=1)
            except rospy.ROSException as e:
                if 'timeout exceeded' in e.message:
                    continue  # no new waypoint within timeout, looping...
                else:
                    raise e
            rospy.loginfo("Recieved waypoints")
            DataLogging('Waypoints received', self.data_publisher)
            PublishWarning(1, 40, 'Ready to start ', 'Ready to start', self.audioWarn_publisher)
            self.poseArray_publisher.publish(waypoints)
        print(waypoints.poses[1])
        userdata.waypoints_out = waypoints
        rospy.loginfo("Recieved waypoints!!!!!")
        # Path is ready! return success and move on to the next state (FOLLOW_PATH)
        return 'success'
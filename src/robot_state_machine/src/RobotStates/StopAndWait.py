#!/usr/bin/env python

import threading
import rospy
import actionlib

from smach import State
from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseArray, PoseStamped
from std_msgs.msg import Empty, Int64, Bool, Header
from actionlib_msgs.msg import GoalID

from Setup_Variables.variables import *

from waypoints_msgs.msg import Waypoints
from warning_message.msg import warning

class StopAndWait(State):
    def __init__(self, start_over):
        State.__init__(self, outcomes=['success'])
        self.hrz_triggered = Bool()
        self.start_over = start_over
        # self.sub = 


    def execute(self, userdata):
        rospy.loginfo('###############################')
        rospy.loginfo('##### HRZ TRIGGERED - DO STH TO STOP THE ROBOT #####')
        rospy.loginfo('###############################')
        # rospy.loginfo('INPUT: %s', (userdata.in_index))
        # DataLogging('HRZ Entered', self.data_publisher)
        hrz_triggered_topic = "/hrz_triggered" 
        self.hrz_triggered.data = True
        # rospy.loginfo("Get into While Loop....")
        rospy.loginfo("Before WHILE I HAVE: %s ", (self.hrz_triggered.data))
        while self.hrz_triggered.data != False:
            try:
                self.hrz_triggered =  rospy.wait_for_message(hrz_triggered_topic, Bool, timeout=1)
                rospy.loginfo("INSIDE WHILE")
                if self.hrz_triggered.data==False:
                    rospy.loginfo("OUT WHILE")
                    # DataLogging('HRZ Exited', self.data_publisher)
                    self.start_over = True
                    # userdata.out_index = userdata.in_index
                    return 'success'
            except rospy.ROSException as e:
                if 'timeout exceeded' in e.message:
                    continue  # no new waypoint within timeout, looping...
                else:
                    raise e
            # rospy.loginfo("I receive: %s", (self.hrz_triggered.data))
        return 'success' 
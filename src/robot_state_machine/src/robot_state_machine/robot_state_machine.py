#!/usr/bin/env python

import threading
import rospy
import actionlib

import os
import subprocess

from smach import State, StateMachine, Concurrence, Container, UserData
from move_base_msgs.msg import MoveBaseAction, MoveBaseActionGoal, MoveBaseGoal
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseArray, PoseStamped
from std_msgs.msg import Empty, Int64, Bool, Header, String
from actionlib_msgs.msg import GoalID

from waypoints_msgs.msg import Waypoints
from warning_message.msg import warning 

from smach_ros import MonitorState, IntrospectionServer 

from WarningPublisher.warnings_publisher import PublishWarning, WarningsPublisher
from DataLogger.DataLogger_publisher import DataLogging, DataPublisher

from math import sqrt

from Setup_Variables.variables import *

# Import PATROL states
from RobotStates.GetPath import GetPath
from RobotStates.FollowPath import FollowPath
from RobotStates.SkipWaypoint import SkipWaypoint
from RobotStates.Stop import Stop
from RobotStates.PathComplete import PathComplete

# Import HZR states
from RobotStates.StopAndWait import StopAndWait

# Import TELEOPERATION states
from RobotStates.Teleoperation import Teleoperation

# Import GET_COFFEE states
from RobotStates.GoToCoffeeMachine import GoToCoffeeMachine
from RobotStates.CoffeeTeleoperation import CoffeeTeleoperation

# Import BED_RAILING states
from RobotStates.GoInFrontOfBed import GoInFrontOfBed
from RobotStates.AutoLiftRailing import AutoLiftRailing
from RobotStates.BedTeleop import BedTeleop

# Import HUMAN_INTERACTION states
from RobotStates.HumanInteraction.FaceRecognition import FaceRecognition as FaceRecog
from RobotStates.HumanInteraction.HumanApproach import HumanApproach as HumanApproach

class Nightcare():
    
    def __init__(self):
        rospy.init_node('nightcare_robot')
        
        
        # Initialize variables
        setup_task_variables(self)
        waypoints = self.waypoints


        # Initialize the state machine

        self.last_nav_state = None
        self.make_coffee_last_nav_state = None
        self.bed_rail_last_nav_state = None
        self.human_int_last_nav_state = None
    
        ###### Create the NAV State machine ######
        
        self.sm_nav = StateMachine(outcomes=['success','start_over','object_moved','out'])
        self.sm_nav.userdata.sm_nav_waypoints = waypoints
        self.sm_nav.userdata.sm_last_waypoint_index = 0
        self.sm_nav.userdata.sm_next_waypoint_index = 0
        

        with self.sm_nav:
            StateMachine.add('GET_PATH', GetPath(self.start_over),
                               transitions={'success':'FOLLOW_PATH',},
                               remapping={'waypoints_in':'sm_nav_waypoints',
                                          'waypoints_out':'sm_nav_waypoints'})
            StateMachine.add('FOLLOW_PATH', FollowPath(self.stop_),
                               transitions={'success':'PATH_COMPLETE',
                                            'start_over':'FOLLOW_PATH'},
                               remapping={'FP_waypoints_in':'sm_nav_waypoints',
                                            'next_waypoint_index':'sm_last_waypoint_index',
                                            'last_waypoint_index':'sm_next_waypoint_index',
                                            'cancelled_goal':'sm_last_waypoint_index'}) 
            StateMachine.add('PATH_COMPLETE', PathComplete(),
                               transitions={'success':'GET_PATH'})
            
        # Register a callback function to fire on state transitions within the sm_nav state machine                       
        self.sm_nav.register_transition_cb(self.nav_transition_cb, cb_args=[])

        ###### Create High Risk Zone State Machine #####
        self.sm_hr_zone = StateMachine(outcomes=['success', 'object_moved','stop'])
        
        with self.sm_hr_zone:
            StateMachine.add('STOP_AND_WAIT', StopAndWait(self.start_over), transitions={'success':''})


        ##############################################################
        #################  LIFT_BED_RAIL_TASK  #######################
        ##############################################################

        ###### Create the BED_RAILING State Machine #####
        self.sm_bed_railing = StateMachine(outcomes=['success','out'])

        with self.sm_bed_railing:
            StateMachine.add('GO_IN_FRONT_OF_BED', GoInFrontOfBed(),transitions={'success':'AUTO_LIFT_RAILING','need_help':'BED_TELEOP'})
            StateMachine.add('AUTO_LIFT_RAILING', AutoLiftRailing(),transitions={'success':''})
            StateMachine.add('BED_TELEOP',BedTeleop(),transitions={'success':''})

        self.sm_bed_railing.register_transition_cb(self.bed_rail_transition_cb, cb_args=[])
        
        ##### Create the BED_MANIP State Machine as a SMACH Concurrence Container  ######
        self.sm_bed_manip = Concurrence(outcomes=['success','object_moved'],
                                          default_outcome = 'success',
                                          child_termination_cb=self.bedManip_child_termination_cb,
                                          outcome_cb=self.bedManip_outcome_cb)
        with self.sm_bed_manip:
            Concurrence.add('BED_RAILING', self.sm_bed_railing)
            Concurrence.add('MONITOR_HRZ', MonitorState("hrz_triggered",Bool, self.hrz_cb))
        
        ################# 
        self.sm_LiftBedRail_task = StateMachine(outcomes=['success'])

        with self.sm_LiftBedRail_task:
            StateMachine.add('BED_MANIP',self.sm_bed_manip,transitions={'object_moved':'OBJECT_MOVED', 'success':''})
            StateMachine.add('OBJECT_MOVED', StopAndWait(self.start_over),transitions={'success':'BED_MANIP'})

        ##########################################################################################################
        ########################################################################################################## 
        ##### END OF LIFT_BED_RAIL_TASK ###### 


        ##############################################################
        #####################  COFFEE_TASK  ##########################
        ##############################################################

        ###### Create the GET_COFFEE State Machine #####
        self.sm_get_coffee = StateMachine(outcomes=['success','out'])

        with self.sm_get_coffee:
            StateMachine.add('GO_TO_COFFEE_MACHINE', GoToCoffeeMachine(),transitions={'success':'COFFEE_TELEOPERATION','need_help':'COFFEE_TELEOPERATION'})
            StateMachine.add('COFFEE_TELEOPERATION', CoffeeTeleoperation(),transitions={'success':''})
        
        self.sm_get_coffee.register_transition_cb(self.make_coffee_transition_cb, cb_args=[])
       
        ##### Create the MAKE_COFFEE State Machine as a SMACH Concurrence Container  ######
        self.sm_make_coffee = Concurrence(outcomes=['success','object_moved'],
                                          default_outcome = 'success',
                                          child_termination_cb=self.makeCoffee_child_termination_cb,
                                          outcome_cb=self.makeCoffee_outcome_cb)
        with self.sm_make_coffee:
            Concurrence.add('GET_COFFEE', self.sm_get_coffee)
            Concurrence.add('MONITOR_HRZ', MonitorState("hrz_triggered",Bool, self.hrz_cb))

       ################# 
        self.sm_Coffee_task = StateMachine(outcomes=['success'])

        with self.sm_Coffee_task:
            StateMachine.add('MAKE_COFFEE',self.sm_make_coffee,transitions={'object_moved':'OBJECT_MOVED', 'success':''})
            StateMachine.add('OBJECT_MOVED', StopAndWait(self.start_over),transitions={'success':'MAKE_COFFEE'})

       ##########################################################################################################
       ########################################################################################################## 
       ##### END OF COFFEE_TASK ###### 


       ############################################################
       #################  NIGHTCARE_TASK  #####################
       ############################################################
       
       ###### Create the HUMAN_INTERACTION State Machine #####
        self.sm_human_interaction = StateMachine(outcomes=['success','abort','need_help','out'])
 
        with self.sm_human_interaction:
            StateMachine.add('APPROACH_HUMAN', HumanApproach(),transitions={'success':'FACE_RECOGNITION','abort':'','need_help':'',})
            StateMachine.add('FACE_RECOGNITION', FaceRecog(),transitions={'success':'','abort':'','need_help':''})
            
 
        self.sm_human_interaction.register_transition_cb(self.human_int_transition_cb, cb_args=[])     
 
       ##### Create the HUMAN_INT State Machine as a SMACH Concurrence Container  ######
        self.sm_human_int = Concurrence(outcomes=['success','object_moved','need_help', 'abort'],
                                           default_outcome = 'success',
                                           child_termination_cb=self.humanInt_child_termination_cb,
                                           outcome_cb=self.humanInt_outcome_cb)
        with self.sm_human_int:
             Concurrence.add('HUMAN_INTERACTION', self.sm_human_interaction)
             Concurrence.add('MONITOR_HRZ', MonitorState("hrz_triggered",Bool, self.hrz_cb))

       ################# 
        self.sm_Nightcare_task = StateMachine(outcomes=['success','need_help','abort'])

        with self.sm_Nightcare_task:
            StateMachine.add('HUMAN_INT',self.sm_human_int, transitions={'object_moved':'OBJECT_MOVED', 'success':'','abort':'','need_help':''})
            StateMachine.add('OBJECT_MOVED', StopAndWait(self.start_over),transitions={'success':'HUMAN_INT'})
        

       ##########################################################################################################
       ########################################################################################################## 
       ##### END OF NIGHTCARE_TASK ###### 


       ##############################################################
       #####################  PATROL  ###############################
       ##############################################################

        ##### Create the PATROL State Machine as a SMACH Concurrence Container  ######

        self.sm_patrol = Concurrence(outcomes=['success','object_moved','stop','still','teleop','get_coffee','bed_found','human_found'], 
                                    default_outcome = 'success',
                                    child_termination_cb=self.concurrence_child_termination_cb,
                                    outcome_cb=self.concurrence_outcome_cb)
                           
        with self.sm_patrol:
            Concurrence.add('NAV', self.sm_nav)
            Concurrence.add('MONITOR_HRZ', MonitorState("hrz_triggered",Bool, self.hrz_cb))
            Concurrence.add('GOAL_BLOCKED', MonitorState("goal_is_cancelled",Bool, self.goal_blocked_cb))
            Concurrence.add('I_WANT_COFFEE', MonitorState("i_want_coffee",Bool, self.i_want_coffee_cb))
            Concurrence.add('I_FOUND_A_BED', MonitorState("i_found_a_bed",Bool, self.i_found_a_bed_cb))
            Concurrence.add('TELEOP', MonitorState("teleop",Bool, self.teleop_cb))
            Concurrence.add('GO_TO_HUMAN', MonitorState("i_saw_a_human",Bool, self.teleop_cb))
        
        ##########################################################################################################
        ########################################################################################################## 
        ##### END OF PATROL ###### 


        ##############################################################
        #####################  TOP LEVEL #############################
        ##############################################################

        ######### Create the TOP LEVEL state machine ######### 
        self.sm_top = StateMachine(outcomes=['success'])
        self.sm_top.userdata.waypoints_ = self.sm_nav.userdata.sm_nav_waypoints

        with self.sm_top:

            StateMachine.add('PATROL',self.sm_patrol,transitions={'success':'PATROL', 
                                                                 'object_moved':'HR_ZONE', 
                                                                 'stop':'STOP',
                                                                 'still':'SKIP_WAYPOINT',
                                                                 'teleop':'TELEOPERATION',
                                                                 'get_coffee':'COFFEE_TASK', 
                                                                 'bed_found':'LIFT_BED_RAIL_TASK',
                                                                 'human_found':'NIGHTCARE_TASK'})
            StateMachine.add('COFFEE_TASK',self.sm_Coffee_task,transitions={'success':'PATROL'})
            StateMachine.add('LIFT_BED_RAIL_TASK',self.sm_LiftBedRail_task,transitions={'success':'PATROL'})
            StateMachine.add('NIGHTCARE_TASK',self.sm_Nightcare_task,transitions={'success':'PATROL',
                                                                                  'need_help':'TELEOPERATION',
                                                                                  'abort':'PATROL'})
            StateMachine.add('HR_ZONE',self.sm_hr_zone, 
                                transitions={'success':'PATROL','object_moved':'HR_ZONE', 'stop':'STOP'})
            StateMachine.add('SKIP_WAYPOINT', SkipWaypoint(self.start_over),
                               transitions={'success':'PATROL',
                                            'object_moved':'HR_ZONE',
                                            'teleop':'TELEOPERATION',
                                            'get_coffee':'COFFEE_TASK',
                                            'bed_found':'LIFT_BED_RAIL_TASK',
                                            'human_found':'NIGHTCARE_TASK'},
                               remapping={'SW_waypoints_in':'waypoints_',
                                          'last_waypoint_index':'sm_next_waypoint_index',
                                          'next_waypoint_index':'sm_last_waypoint_index'})
            StateMachine.add('TELEOPERATION', Teleoperation(), transitions={'success':'PATROL'})                              
            StateMachine.add('STOP', Stop(), transitions={'success':'PATROL'})
            
        #####################################################################################
        # # Create and start the introspection server
        sis = IntrospectionServer('NightCare', self.sm_top, '/SM_ROOT')
        sis.start()
    
        ## Execute the state machine
        outcome = self.sm_top.execute()

        # # Wait for ctrl-c to stop the application
        rospy.spin()
        sis.stop()
###########################################################################################

###########################################################################################
################## COFFEE_TASK OUTCOME REASONING ##########################################
###########################################################################################

    def makeCoffee_child_termination_cb(self,outcome_map):
        if outcome_map['GET_COFFEE'] == 'out':
            rospy.loginfo("COFFEE OUT")
            if self.make_coffee_last_nav_state is not None:
                 self.sm_get_coffee.set_initial_state(['GO_TO_COFFEE_MACHINE'] , UserData())
            return True
        if outcome_map['GET_COFFEE'] == 'success':
            rospy.loginfo("COFFEE SUCCESS")
            if self.make_coffee_last_nav_state is not None:
                 self.sm_get_coffee.set_initial_state(['GO_TO_COFFEE_MACHINE'], UserData())
            return True
        if outcome_map['MONITOR_HRZ'] == 'invalid':
            rospy.loginfo("COFFEE OBJECT MOVED! NEED TO STOP...")
            if self.make_coffee_last_nav_state is not None:
                 self.sm_get_coffee.set_initial_state(['GO_TO_COFFEE_MACHINE'], UserData())
            return True
        else:
            return False
    
    def makeCoffee_outcome_cb(self, outcome_map):     
        if outcome_map['GET_COFFEE'] == 'out':
            rospy.loginfo("GET_COFFEE OUT! NEED TO STOP...")
            return 'object_moved'
        elif outcome_map['GET_COFFEE'] == 'success':
            rospy.loginfo("COFFEE MADE")
            return 'success'
        elif outcome_map['MONITOR_HRZ'] == 'invalid':
            rospy.loginfo("COFFEE HRZ_INVALID! NEED TO STOP...")
            return 'object_moved'
        else:
            rospy.loginfo("WE GO ON")
            return 'success' 

    def make_coffee_transition_cb(self, userdata, active_states, *cb_args):
        print(" State : %s", (active_states))
        self.make_coffee_last_nav_state = active_states

###########################################################################################
################## LIFT_BED_RAIL_TASK OUTCOME REASONING ##########################################
###########################################################################################

    def bedManip_child_termination_cb(self,outcome_map):
        if outcome_map['BED_RAILING'] == 'out':
            rospy.loginfo("BED_MANIP OUT")
            if self.bed_rail_last_nav_state is not None:
                 self.sm_bed_railing.set_initial_state(['GO_IN_FRONT_OF_BED'] , UserData())
            return True
        if outcome_map['BED_RAILING'] == 'success':
            rospy.loginfo("BED_MANIP SUCCESS")
            if self.bed_rail_last_nav_state is not None:
                 self.sm_bed_railing.set_initial_state(['GO_IN_FRONT_OF_BED'], UserData())
            return True
        if outcome_map['MONITOR_HRZ'] == 'invalid':
            rospy.loginfo("BED OBJECT MOVED! NEED TO STOP...")
            if self.bed_rail_last_nav_state is not None:
                 self.sm_bed_railing.set_initial_state(['GO_IN_FRONT_OF_BED'], UserData())
            return True
        else:
            return False
    
    def bedManip_outcome_cb(self, outcome_map):     
        if outcome_map['BED_RAILING'] == 'out':
            rospy.loginfo("BED_RAILING OUT! NEED TO STOP...")
            return 'object_moved'
        elif outcome_map['BED_RAILING'] == 'success':
            rospy.loginfo("BED_RAILING MADE")
            return 'success'
        elif outcome_map['MONITOR_HRZ'] == 'invalid':
            rospy.loginfo("BED HRZ_INVALID! NEED TO STOP...")
            return 'object_moved'
        else:
            rospy.loginfo("WE GO ON")
            return 'success' 

    def bed_rail_transition_cb(self, userdata, active_states, *cb_args):
        print(" State : %s", (active_states))
        self.bed_rail_last_nav_state = active_states


###########################################################################################
################## NIGHTCARE_TASK OUTCOME REASONING ##########################################
###########################################################################################

    def humanInt_child_termination_cb(self,outcome_map):
        if outcome_map['HUMAN_INTERACTION'] == 'out':
            rospy.loginfo("HUMAN OUT")
            if self.human_int_last_nav_state is not None:
                 self.sm_human_interaction.set_initial_state(['APPROACH_HUMAN'] , UserData())
            return True
        if outcome_map['HUMAN_INTERACTION'] == 'success':
            rospy.loginfo("HUMAN SUCCESS")
            if self.human_int_last_nav_state is not None:
                 self.sm_human_interaction.set_initial_state(['APPROACH_HUMAN'], UserData())
            return True
        if outcome_map['HUMAN_INTERACTION'] == 'abort':
            rospy.loginfo("HUMAN ABORT")
            if self.human_int_last_nav_state is not None:
                 self.sm_human_interaction.set_initial_state(['APPROACH_HUMAN'], UserData())
            return True
        if outcome_map['MONITOR_HRZ'] == 'invalid':
            rospy.loginfo("COFFEE OBJECT MOVED! NEED TO STOP...")
            if self.human_int_last_nav_state is not None:
                 self.sm_human_interaction.set_initial_state(['APPROACH_HUMAN'], UserData())
            return True
        else:
            return False
    
    def humanInt_outcome_cb(self, outcome_map):     
        if outcome_map['HUMAN_INTERACTION'] == 'out':
            rospy.loginfo("HUMAN INTERACTION OUT! NEED TO STOP...")
            return 'object_moved'
        elif outcome_map['HUMAN_INTERACTION'] == 'success':
            rospy.loginfo("HUMAN INTERACTION COMPLETED")
            return 'success'
        elif outcome_map['HUMAN_INTERACTION'] == 'abort':
            rospy.loginfo("HUMAN INTERACTION ABORTED")
            return 'abort'
        elif outcome_map['MONITOR_HRZ'] == 'invalid':
            rospy.loginfo("HUMAN INTERACTION INVALID! NEED TO STOP...")
            return 'object_moved'
        else:
            rospy.loginfo("WE GO ON")
            return 'success' 

    def human_int_transition_cb(self, userdata, active_states, *cb_args):
        print(" State : %s", (active_states))
        self.human_int_last_nav_state = active_states


###########################################################################################
####################### PATROL OUTCOME REASONING ##########################################
###########################################################################################


    def concurrence_child_termination_cb(self,outcome_map):
        
        if outcome_map['NAV'] == 'interrupt_skip':
            rospy.loginfo("HRZ FCK GOAL...")
            return True
        if outcome_map['NAV'] == 'success':
            rospy.loginfo("NEXT STATE: STOP")
            rospy.sleep(2.)
            return True
        # If the MonitorState state returns False (invalid), store the current nav goal and trigger stop and wait behaviour
        if outcome_map['MONITOR_HRZ'] == 'invalid':
            rospy.loginfo("OBJECT MOVED! NEED TO STOP...")
            if self.last_nav_state is not None:
                 self.sm_nav.set_initial_state(self.last_nav_state, UserData())
            return True
        # If the MonitorState state returns False (invalid), store the current nav goal and trigger stop and wait behaviour
        if outcome_map['GOAL_BLOCKED'] == 'invalid':
            rospy.loginfo("GOAL IS BLOCKED! NEED TO STOP...")
            if self.last_nav_state is not None:
                 self.sm_nav.set_initial_state(self.last_nav_state, UserData())
            return True
        if outcome_map['TELEOP'] == 'invalid':
            rospy.loginfo("TELEOP IS ON! NEED TO STOP...")
            if self.last_nav_state is not None:
                 self.sm_nav.set_initial_state(self.last_nav_state, UserData())
            return True
        if outcome_map['I_WANT_COFFEE'] == 'invalid':
            rospy.loginfo("I_WANT_COFFEE IS ON! NEED TO STOP...")
            if self.last_nav_state is not None:
                 self.sm_nav.set_initial_state(self.last_nav_state, UserData())
            return True
        if outcome_map['I_FOUND_A_BED'] == 'invalid':
            rospy.loginfo("I_FOUND_A_BED IS ON! NEED TO STOP...")
            if self.last_nav_state is not None:
                 self.sm_nav.set_initial_state(self.last_nav_state, UserData())
            return True
        if outcome_map['GO_TO_HUMAN'] == 'invalid':
            rospy.loginfo("HUMAN_APPROACH IS ON! NEED TO STOP...")
            if self.last_nav_state is not None:
                 self.sm_nav.set_initial_state(self.last_nav_state, UserData())
            return True
        else:
            return False

    def concurrence_outcome_cb(self, outcome_map):
            
            if outcome_map['NAV'] == 'success':
                rospy.loginfo("TEST! NEED TO STOP...")
                return 'stop'
            elif outcome_map['NAV'] == 'interrupt_skip':
                rospy.loginfo("INTERUPT! NEED TO STOP...")
                return 'object_moved'
            elif outcome_map['MONITOR_HRZ'] == 'invalid':
                rospy.loginfo("HRZ_INVALID! NEED TO STOP...")
                return 'object_moved'
            elif outcome_map['GOAL_BLOCKED'] == 'invalid':
                rospy.loginfo("GOAL_BLOCKED_INVALID! NEED TO STOP...")
                return 'still'
            elif outcome_map['TELEOP'] == 'invalid':
                rospy.loginfo("TELEOP_INVALID! NEED TO STOP...")
                return 'teleop'
            elif outcome_map['I_WANT_COFFEE'] == 'invalid':
                rospy.loginfo("I_WANT_COFFEE! NEED TO STOP...")
                return 'get_coffee'
            elif outcome_map['I_FOUND_A_BED'] == 'invalid':
                rospy.loginfo("I_FOUND_A_BED! NEED TO STOP...")
                return 'bed_found'
            elif outcome_map['GO_TO_HUMAN'] == 'invalid':
                rospy.loginfo("GO_TO_HUMAN! NEED TO STOP...")
                return 'human_found'
            else:
                rospy.loginfo("WE GO ON")
                return 'success'       

    def nav_transition_cb(self, userdata, active_states, *cb_args):
        self.last_nav_state = active_states
        self.nav_current_goal = self.sm_nav.userdata.sm_last_waypoint_index
        print(self.nav_current_goal)
        print(self.last_nav_state)

    def hrz_cb(self, userdata, msg):
        pub = rospy.Publisher('/move_base/cancel', GoalID, queue_size=1)
        # rospy.loginfo("GMTX: %s", (msg.data))
        stopGoal_ = GoalID()
        stopGoal_.id = ''
        if msg.data == True:
            self.stop_ = True
            pub.publish(stopGoal_)
            return False # Monitor State will return 'invalid'
        elif msg.data == False:
            self.stop_ = False
            return True # Monitor State will return 'valid'
        else:
            return True

    def goal_blocked_cb(self, userdata, msg):
        pub = rospy.Publisher('/move_base/cancel', GoalID, queue_size=1)
        SkipGoal_ = GoalID()
        SkipGoal_.id = ''
        if msg.data == True:
            pub.publish(SkipGoal_)
            return False # Monitor State will return 'invalid'
        elif msg.data == False:
            return True # Monitor State will return 'valid'
        else:
            return True

    def teleop_cb(self, userdata, msg):
        pub = rospy.Publisher('/move_base/cancel', GoalID, queue_size=1)
        telPoint_ = GoalID()
        telPoint_.id = ''
        if msg.data == True:
            pub.publish(telPoint_)
            return False # Monitor State will return 'invalid'
        elif msg.data == False:
            return True # Monitor State will return 'valid'

    def i_want_coffee_cb(self, userdata, msg):
        pub = rospy.Publisher('/move_base/cancel', GoalID, queue_size=1)
        CoffeePoint_ = GoalID()
        CoffeePoint_.id = ''
        if msg.data == True:
            pub.publish(CoffeePoint_)
            return False # Monitor State will return 'invalid'
        elif msg.data == False:
            return True # Monitor State will return 'valid'
    
    def i_found_a_bed_cb(self, userdata, msg):
        pub = rospy.Publisher('/move_base/cancel', GoalID, queue_size=1)
        BedPoint_ = GoalID()
        BedPoint_.id = ''
        if msg.data == True:
            pub.publish(BedPoint_)
            return False # Monitor State will return 'invalid'
        elif msg.data == False:
            return True # Monitor State will return 'valid'

#!/usr/bin/env bash

### Automate patrolling for Marco ### 

# It includes patrolling, clear costmaps, goal_assessor and High Risk Zone

# Source necessary workspaces #
source /home/cockpit/Desktop/kkh_workspaces/follow_waypoints_ws/devel/setup.bash --extend
source /home/cockpit/Desktop/kkh_workspaces/robot_state_machine_ws/devel/setup.bash --extend
source /home/cockpit/Desktop/kkh_workspaces/human_detection_ws/devel/setup.bash --extend

# rosrun robot_state_machine prepare_to_patrol 

# Launch the state machine 
rosrun robot_state_machine robot_state_machine &
sleep 5;

# # Publish the generated waypoints 
rosrun generate_waypoints generate_waypoints_publish_waypoints _file:=/home/cockpit/Desktop/kkh_workspaces/robot_state_machine_ws/hallway.csv &
sleep 3;

# # Start the patrolling 
rostopic pub /path_ready std_msgs/Empty -1 &
sleep 1;

# # Start map clearing
xterm -e rosrun clear_costmap clear_costmap_node &
sleep 0.5;

# # Start goal accessor
rosrun goal_assessor goal_assessor &
sleep 0.5;

# ##########################
# ##### High Risk Zone #####
# ##########################

xterm -e rosrun simple_laser_velocity_detection simple_laser_velocity_detection &
sleep 1;
xterm -e roslaunch srl_nearest_neighbor_tracker nnt_laser.launch &
# sleep 1;
# xterm -e rosrun spencer_msgs_convert spencer_msgs_convert &
sleep 1;
xterm -e rosrun safety_zones safety_zones_high_risk_zone _hrz_radius:=0.7 _wait_for_goal_time_:=5.0 



# # To stop control+C
# trap "echo Test stopped!; exit;" SIGINT SIGTERM
#!/usr/bin/env bash

# Perform a head motion 

# Load head_controller #
rosservice call /controller_manager/load_controller "name: 'head_controller'"
# Start head_controller #
rosservice call /controller_manager/switch_controller "{start_controllers: ['head_controller']}"

## Do something with head ##
# Look down
rostopic pub /head_controller/command trajectory_msgs/JointTrajectory "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
joint_names:
 ['head_1_joint', 'head_2_joint']
points:
- positions: [0,-0.8]
  velocities: []
  accelerations: []
  effort: []
  time_from_start: {secs: 2, nsecs: 0}" --once

sleep 2

  rostopic pub /head_controller/command trajectory_msgs/JointTrajectory "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
joint_names:
 ['head_1_joint', 'head_2_joint']
points:
- positions: [0,0]
  velocities: []
  accelerations: []
  effort: []
  time_from_start: {secs: 2, nsecs: 0}" --once

sleep 1

# Stop head_controller
rosservice call /controller_manager/switch_controller "{stop_controllers: ['head_controller']}"
 
# Unload head_controller #
rosservice call /controller_manager/unload_controller "name: 'head_controller'"


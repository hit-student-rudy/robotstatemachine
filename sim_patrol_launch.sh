#!/usr/bin/env bash

### Automate patrolling for Marco ### 

# It includes patrolling, clear costmaps, goal_assessor and High Risk Zone

# Source necessary workspaces #
#source /home/cockpit/Desktop/kkh_workspaces/follow_waypoints_ws/devel/setup.bash --extend
#source /home/cockpit/Desktop/kkh_workspaces/robot_state_machine_ws/devel/setup.bash --extend
# source /home/cockpit/Desktop/kkh_workspaces/human_detection_ws/devel/setup.bash --extend

source /home/dimitris/workspaces/ros_websockets_ws/devel/setup.bash --extend


xterm -e rosrun websockets_server websockets_server_gui &
sleep 0.5;

xterm -e gst-launch-1.0 --gst-debug 4 udpsrc port=5052 caps=application/x-rtp ! rtpjitterbuffer ! rtpopusdepay ! opusdec ! autoaudiosink &

python3 /home/dimitris/workspaces/robot_state_machine_ws/gui/simple_gui.py &
sleep 10;
# Launch the state machine 
xterm -e rosrun robot_state_machine robot_state_machine &
sleep 3;

# Publish the generated waypoints 
xterm -e rosrun generate_waypoints generate_waypoints_publish_waypoints _file:=/home/dimitris/workspaces/follow_waypoints_ws/sim_waypoints.csv &
sleep 2;

# Start the patrolling 
xterm -e rostopic pub /path_ready std_msgs/Empty -1 &
sleep 1;

# Start map clearing
rosrun clear_costmap clear_costmap_node &
sleep 0.5;

# Start goal accessor
rosrun goal_assessor goal_assessor 


##########################
##### High Risk Zone #####
##########################

# xterm -e rosrun simple_laser_velocity_detection simple_laser_velocity_detection &
# sleep 1;
# xterm -e roslaunch srl_nearest_neighbor_tracker nnt_laser.launch &
# sleep 1;
# xterm -e rosrun spencer_msgs_convert spencer_msgs_convert &
# sleep 1;
# xterm -e rosrun safety_zones safety_zones_high_risk_zone _hrz_radius:=0.7 _wait_for_goal_time_:=5.0 



# To stop control+C
trap "echo Test stopped!; exit;" SIGINT SIGTERM

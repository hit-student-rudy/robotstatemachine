#!/bin/bash

# To stop control+C
trap "echo Exited!; exit;" SIGINT SIGTERM

# Load head_controller #
rosservice call /controller_manager/load_controller "name: 'head_controller'"
sleep 1;
# Start head_controller #
rosservice call /controller_manager/switch_controller "{start_controllers: ['head_controller']}"

sleep 2;
rostopic pub /head_controller/command trajectory_msgs/JointTrajectory "header:
  seq: 0
  stamp:
    secs: 0
    nsecs: 0
  frame_id: ''
joint_names:
 ['head_1_joint', 'head_2_joint']
points:
- positions: [0,-0.3]
  velocities: []
  accelerations: []
  effort: []
  time_from_start: {secs: 1, nsecs: 0}" --once

  sleep 3;

rosservice call /controller_manager/switch_controller "{stop_controllers: ['head_controller']}"

sleep 1;

rosservice call /controller_manager/unload_controller "name: 'head_controller'"

